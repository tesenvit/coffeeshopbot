<?php

use Illuminate\Database\Seeder;

class BotTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bot')->insert([
            [
                'active' => false,
                'text_command_start' => 'Lorem Ipsum🙈
is simply dummy 🙉
text of the printing🙊
<b>and typesetting industry.</b>
<i>Lorem Ipsum has been</i>
industry\'s <a href="https://google.com">standard</a> dummy text ever since the 1500s,
<code>when an unknown printer took</code>
<pre>a galley of type and scrambled it to make</pre>
a type specimen book.',
                'number_of_buttons' => 2
            ]
        ]);
    }
}
