<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(CitiesTableSeeder::class);
         $this->call(DistrictsTableSeeder::class);
         $this->call(ProductsTableSeeder::class);
         $this->call(RealizationTableSeeder::class);
         $this->call(BotTableSeeder::class);
         $this->call(UsersTableSeeder::class);
    }
}
