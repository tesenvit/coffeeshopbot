<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cities')->insert([
            [
                'title' => 'Королевская Гавань',
            ],
            [
                'title' => 'Старомест',
            ],
            [
                'title' => 'Винтерфелл',
            ]
        ]);
    }
}
