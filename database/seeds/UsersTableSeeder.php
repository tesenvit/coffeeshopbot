<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'username' => 'administrator',
                'password' => bcrypt('password'),
                'role' => 'admin',
                'created_at' => Carbon::now(),
                'updated_at' =>  Carbon::now()
            ],
            [
                'username' => 'Black Mamba',
                'password' => bcrypt('password'),
                'role' => 'moder',
                'created_at' => Carbon::now(),
                'updated_at' =>  Carbon::now()
            ],
            [
                'username' => 'yellow_submarine',
                'password' => bcrypt('password'),
                'role' => 'moder',
                'created_at' => Carbon::now(),
                'updated_at' =>  Carbon::now()
            ],
        ]);
    }
}
