<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            [
                'title' => 'Арабика 5кг',
                'price' => 1000,
            ],
            [
                'title' => 'Арабика 2кг',
                'price' => 500,
            ],
            [
                'title' => 'Арабика 0,5кг',
                'price' => 300,
            ],
            [
                'title' => 'Робуста 5кг',
                'price' => 950,
            ],
            [
                'title' => 'Робуста 2кг',
                'price' => 475,
            ],
            [
                'title' => 'Робуста 0,5кг',
                'price' => 275,
            ],
        ]);
    }
}
