<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

class RealizationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('realization')->insert([
            [
                'user_id' => 2,
                'city_id' => 1,
                'district_id' => 1,
                'product_id' => 1,
                'image' => '0VodR1gRzgFQ4XlPsaykcx5JTT6VObUZ4MBvOekv.jpeg',
                'thumbnail' => 'thumbnails/0VodR1gRzgFQ4XlPsaykcx5JTT6VObUZ4MBvOekv.jpeg',
                'order_key' => '5a219bcd6c633',
                'reserve' => false,
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy',
                'created_at' => Carbon::now()->modify('-1 second'),
                'updated_at' => Carbon::now()->modify('-1 second')
            ],
            [
                'user_id' => 2,
                'city_id' => 1,
                'district_id' => 1,
                'product_id' => 1,
                'image' => '2hk88K6ix2vD0gPnEDwzFBnV5DJiiqUcrpvYDFz4.jpeg',
                'thumbnail' => 'thumbnails/2hk88K6ix2vD0gPnEDwzFBnV5DJiiqUcrpvYDFz4.jpeg',
                'order_key' => '5a219bcd6c634',
                'reserve' => false,
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy',
                'created_at' => Carbon::now()->modify('-1 minute'),
                'update_at' => Carbon::now()->modify('-1 minute')
            ],
            [
                'user_id' => 2,
                'city_id' => 1,
                'district_id' => 3,
                'product_id' => 2,
                'image' => '9OpjJVqGKnIgThvp9beKvO2v9p3zkDQyB1ABLCYg.jpeg',
                'thumbnail' => 'thumbnails/9OpjJVqGKnIgThvp9beKvO2v9p3zkDQyB1ABLCYg.jpeg',
                'order_key' => '5a219bcd8c633',
                'reserve' => false,
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy',
                'created_at' => Carbon::now()->modify('-10 minute'),
                'updated_at' => Carbon::now()->modify('-10 minute')
            ],
            [
                'user_id' => 2,
                'city_id' => 1,
                'district_id' => 1,
                'product_id' => 2,
                'image' => 'an96iKP1T6rDTN9UOHLFD6dAmBe7Dphdi9YhLyuG.jpeg',
                'thumbnail' => 'thumbnails/an96iKP1T6rDTN9UOHLFD6dAmBe7Dphdi9YhLyuG.jpeg',
                'order_key' => '5a229bcd6c633',
                'reserve' => false,
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy',
                'created_at' => Carbon::now()->modify('-1 hour'),
                'updated_at' => Carbon::now()->modify('-1 hour')
            ],
            [
                'user_id' => 2,
                'city_id' => 1,
                'district_id' => 1,
                'product_id' => 3,
                'image' => 'bsMCTwcWHPzB2K6X5DKw7WJcL4GVJfL9tRwsEAVj.jpeg',
                'thumbnail' => 'thumbnails/bsMCTwcWHPzB2K6X5DKw7WJcL4GVJfL9tRwsEAVj.jpeg',
                'order_key' => '5a219bcd6c690',
                'reserve' => false,
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy',
                'created_at' => Carbon::now()->modify('-5 hour'),
                'updated_at' => Carbon::now()->modify('-5 hour')
            ],
            [
                'user_id' => 2,
                'city_id' => 1,
                'district_id' => 1,
                'product_id' => 4,
                'image' => 'c7wCC8HMzN6mZhOIUmRyAw0aFquVpBN2FkzNq4xD.jpeg',
                'thumbnail' => 'thumbnails/c7wCC8HMzN6mZhOIUmRyAw0aFquVpBN2FkzNq4xD.jpeg',
                'order_key' => '5a219bcd6c699',
                'reserve' => false,
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy',
                'created_at' => Carbon::now()->modify('-10 hour'),
                'updated_at' => Carbon::now()->modify('-10 hour')
            ],
            //
            [
                'user_id' => 2,
                'city_id' => 2,
                'district_id' => 7,
                'product_id' => 5,
                'image' => 'CM7XRic2JDxSu2R8natTI1y4yDvbkTVucSSPYM6R.jpeg',
                'thumbnail' => 'thumbnails/CM7XRic2JDxSu2R8natTI1y4yDvbkTVucSSPYM6R.jpeg',
                'order_key' => '5b219bcd6c633',
                'reserve' => false,
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy',
                'created_at' => Carbon::now()->modify('-30 second'),
                'updated_at' => Carbon::now()->modify('-30 second')
            ],
            [
                'user_id' => 2,
                'city_id' => 1,
                'district_id' => 2,
                'product_id' => 5,
                'image' => 'iOl9G7edQh3tuCQx2aPmP1qKLUbPxwEnIqA82xYT.jpeg',
                'thumbnail' => 'thumbnails/iOl9G7edQh3tuCQx2aPmP1qKLUbPxwEnIqA82xYT.jpeg',
                'order_key' => '5a219acd6c633',
                'reserve' => false,
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy',
                'created_at' => Carbon::now()->modify('-2 hour'),
                'updated_at' => Carbon::now()->modify('-2 hour')
            ],
            [
                'user_id' => 3,
                'city_id' => 1,
                'district_id' => 2,
                'product_id' => 6,
                'image' => 'NZoCVXuVOKjOgGzFVmoGYYOAkNObukU8PHCYmRG1.jpeg',
                'thumbnail' => 'thumbnails/NZoCVXuVOKjOgGzFVmoGYYOAkNObukU8PHCYmRG1.jpeg',
                'order_key' => '1a219bcd6c633',
                'reserve' => false,
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy',
                'created_at' => Carbon::now()->modify('-12 hour'),
                'updated_at' => Carbon::now()->modify('-12 hour')
            ],
            [
                'user_id' => 3,
                'city_id' => 2,
                'district_id' => 6,
                'product_id' => 6,
                'image' => 'qfJYtGPQWHn2HZ9dZTdYdrMWdyvNT8iBnZLtzuEC.jpeg',
                'thumbnail' => 'thumbnails/qfJYtGPQWHn2HZ9dZTdYdrMWdyvNT8iBnZLtzuEC.jpeg',
                'order_key' => '2a219bcd6c633',
                'reserve' => false,
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy',
                'created_at' => Carbon::now()->modify('-16 hour'),
                'updated_at' => Carbon::now()->modify('-16 hour')
            ],
            [
                'user_id' => 3,
                'city_id' => 1,
                'district_id' => 2,
                'product_id' => 3,
                'image' => 'x8IJaNSEVoxFVCWZsCeYJrhF4fKN3cEKd8IgVpfP.jpeg',
                'thumbnail' => 'thumbnails/x8IJaNSEVoxFVCWZsCeYJrhF4fKN3cEKd8IgVpfP.jpeg',
                'order_key' => '5a719bcd6c633',
                'reserve' => false,
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy',
                'created_at' => Carbon::now()->modify('-1 day'),
                'updated_at' => Carbon::now()->modify('-1 day')
            ],
            [
                'user_id' => 3,
                'city_id' => 2,
                'district_id' => 5,
                'product_id' => 3,
                'image' => 'XSbXh92oFDcsXmy3DGBQqTYcRSQbVv1sd2lFKkjW.jpeg',
                'thumbnail' => 'thumbnails/XSbXh92oFDcsXmy3DGBQqTYcRSQbVv1sd2lFKkjW.jpeg',
                'order_key' => '5a219bgt6c633',
                'reserve' => false,
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy',
                'created_at' => Carbon::now()->modify('-2 day'),
                'updated_at' => Carbon::now()->modify('-2 day')
            ],
        ]);

    }
}
