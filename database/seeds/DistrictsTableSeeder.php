<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DistrictsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('districts')->insert([
            [
                'title' => 'Красный замок',
                'city_id' => 1
            ],
            [
                'title' => 'Септа Бейелор',
                'city_id' => 1
            ],
            [
                'title' => 'Гильдия Алхимистов',
                'city_id' => 1
            ],
            [
                'title' => 'Блошиное Дно',
                'city_id' => 1
            ],
            //
            [
                'title' => 'Цитадель',
                'city_id' => 2
            ],
            [
                'title' => 'Высокая башня',
                'city_id' => 2
            ],
            [
                'title' => 'Звездная септа',
                'city_id' => 2
            ],
        ]);
    }
}
