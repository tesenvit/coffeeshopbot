<?php
/*
+---------------------------------------------------------------------+
|                                                                     |
|                       AUTH AND REGISTER START                       |
|                                                                     |
+---------------------------------------------------------------------+
*/
// Authentication routes
Route::get('/',
[
  'as' => 'login',
  'uses' => 'Auth\LoginController@showLoginForm'
]);

Route::post('/',
[
  'as' => '',
  'uses' => 'Auth\LoginController@login'
]);

Route::post('logout',
[
  'as' => 'logout',
  'uses' => 'Auth\LoginController@logout'
]);

// Registration admin routes
Route::get('register',
[
    'as' => 'register',
    'uses' => 'Auth\RegisterController@showRegistrationForm',
    'middleware' => 'registerAdmin'
]);

Route::post('register',
[
    'as' => '',
    'uses' => 'Auth\RegisterController@register',
    'middleware' => 'registerAdmin'
]);

/*
+---------------------------------------------------------------------+
|                                                                     |
|                       AUTH AND REGISTER STOP                        |
|                                                                     |
+---------------------------------------------------------------------+
*/


/*
+---------------------------------------------------------------------+
|                                                                     |
|                        ADMIN DASHBOARD START                        |
|                                                                     |
+---------------------------------------------------------------------+
*/
Route::group(['middleware' => ['auth','admin'], 'prefix' => 'admin'], function()
{
    // Main page
    Route::get('/',
    [
        'as' => 'admin-dashboard',
        'uses' => 'User\AdminController@index'
    ]);

    // Date picker
    Route::post('/',
    [
        'as' => '',
        'uses' => 'User\AdminController@index'
    ]);

/*
+---------------------------------------------------------------+
|                            PROFILE                            |
+---------------------------------------------------------------+
*/
    // Show form for change of password and username
    Route::get('profile',
    [
        'as' => 'profile',
        'uses' => 'User\AdminController@profileShow'
    ]);

    Route::put('profile',
    [
        'as' => 'update-profile',
        'uses' => 'User\AdminController@updateProfile'
    ]);

/*
+---------------------------------------------------------------+
|                             LISTS                             |
+---------------------------------------------------------------+
*/
    // list of orders
    Route::get('list-orders',
    [
        'as' => 'list-orders',
        'uses' => 'User\AdminController@listOrders'
    ]);

    // list of realization
    Route::get('list-realization',
    [
        'as' => 'list-realization',
        'uses' => 'User\AdminController@listRealization'
    ]);

    // list of Telegram users
    Route::get('telegram-users',
    [
        'as' => 'telegram-users',
        'uses' => 'User\AdminController@listTelegramUsers'
    ]);

    // Delete realization
    Route::delete('list-realization',
    [
        'as' => 'delete-realization',
        'uses' => 'User\AdminController@destroyRealization'
    ]);
/*
+---------------------------------------------------------------+
|                             SHOP                              |
+---------------------------------------------------------------+
*/
    Route::group(['prefix' => 'shop'],function(){

        /*
        +----------------------+
        |       CITY CRUD      |
        +----------------------+
        */
        // Show all cities
        Route::get('cities',
        [
            'as' => 'cities',
            'uses' => 'User\AdminController@shopCities'
        ]);

        // Create
        Route::post('cities',
        [
            'as' => 'store-city',
            'uses' => 'User\AdminController@shopStoreCity'
        ]);

        // Update
        Route::put('cities',
        [
            'as' => 'update-city',
            'uses' => 'User\AdminController@shopUpdateCity'
        ]);

        // Delete
        Route::delete('cities',
        [
            'as' => '',
            'uses' => 'User\AdminController@shopDestroyCity'
        ]);

        /*
        +----------------------+
        |    DISTRICT CRUD     |
        +----------------------+
        */
        // Show all districts
        Route::get('districts',
        [
            'as' => 'districts',
            'uses' => 'User\AdminController@shopDistricts'
        ]);

        // Create
        Route::post('districts',
        [
            'as' => 'store-district',
            'uses' => 'User\AdminController@shopStoreDistrict'
        ]);

        // Update
        Route::put('districts',
        [
            'as' => 'update-district',
            'uses' => 'User\AdminController@shopUpdateDistrict'
        ]);

        // Delete
        Route::delete('districts',
        [
            'as' => '',
            'uses' => 'User\AdminController@shopDestroyDistrict'
        ]);

        /*
       +----------------------+
       |     PRODUCT CRUD     |
       +----------------------+
       */
        // Show all districts
        Route::get('products',
        [
            'as' => 'products',
            'uses' => 'User\AdminController@shopProducts'
        ]);

        // Create
        Route::post('products',
        [
            'as' => 'store-product',
            'uses' => 'User\AdminController@shopStoreProduct'
        ]);

        // Update
        Route::put('products',
        [
            'as' => 'update-product',
            'uses' => 'User\AdminController@shopUpdateProduct'
        ]);

        // Delete
        Route::delete('products',
        [
            'as' => '',
            'uses' => 'User\AdminController@shopDestroyProduct'
        ]);
    });

/*
+---------------------------------------------------------------+
|                            MODER                              |
+---------------------------------------------------------------+
*/
    Route::group(['prefix' => 'moder'],function(){

        // Show all moderators
        Route::get('/',
        [
            'as' => 'list-moders',
            'uses' => 'User\AdminController@moderIndex'
        ]);

        // Show concrete moderator
        Route::get('{username}/show',
        [
            'as' => '',
            'uses' => 'User\AdminController@moderShow'
        ]);

        // Form for create
        Route::get('create',
        [
            'as' => 'create-moder',
            'uses' => 'User\AdminController@moderCreate'
        ]);

        // Create
        Route::post('create',
        [
            'as' => 'store-moder',
            'uses' => 'User\AdminController@moderStore'
        ]);

        // Update
        Route::put('update',
        [
            'as' => 'update-moder',
            'uses' => 'User\AdminController@moderUpdate'
        ]);

        // Delete
        Route::delete('{username}/show',
        [
            'as' => '',
            'uses' => 'User\AdminController@moderDestroy'
        ]);
    });

/*
+---------------------------------------------------------------+
|                             BOT                               |
+---------------------------------------------------------------+
*/
    Route::group(['prefix' => 'bot'],function(){

        Route::get('/',
        [
            'as' => 'index-bot',
            'uses' => 'User\AdminController@botIndex'
        ]);

        Route::post('/',
        [
            'as' => 'on-off-bot',
            'uses' => 'BotController@onOffTumbler'
        ]);

        Route::put('/',
        [
            'as' => 'update-text-command-start',
            'uses' => 'User\AdminController@botUpdateCommandStart'
        ]);

        Route::get('send-message',
        [
            'as' => 'show-message-everyone',
            'uses' => 'User\AdminController@botSendMessageEveryone'
        ]);

        Route::post('send-message',
        [
            'as' => 'send-message-everyone',
            'uses' => 'BotController@sendMessageEveryone'
        ]);
    });
});
//
/*
+---------------------------------------------------------------------+
|                                                                     |
|                        ADMIN DASHBOARD STOP                         |
|                                                                     |
+---------------------------------------------------------------------+
*

/*
+---------------------------------------------------------------------+
|                                                                     |
|                            MODER START                              |
|                                                                     |
+---------------------------------------------------------------------+
*/

Route::group(['middleware' => ['auth','moder'], 'prefix' => 'moder'], function()
{
    Route::get('/',
    [
        'as' => 'moder-dashboard',
        'uses' => 'User\ModerController@index'
    ]);

    Route::get('upload',
    [
        'as' => 'form-upload',
        'uses' => 'User\ModerController@upload'
    ]);

    Route::post('upload',
    [
        'as' => 'do-upload',
        'uses' => 'User\ModerController@doUpload'
    ]);

    Route::delete('upload',
    [
        'as' => 'destroy-realization',
        'uses' => 'User\ModerController@destroyRealization'
    ]);

    Route::get('list-districts',
    [
        'as' => '',
        'uses' => 'User\ModerController@listDistricts'
    ]);
});

/*
+---------------------------------------------------------------------+
|                                                                     |
|                            MODER STOP                               |
|                                                                     |
+---------------------------------------------------------------------+
*/



