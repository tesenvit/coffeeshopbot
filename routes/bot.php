<?php
/*
|--------------------------------------------------------------------------
| Set Webhook
|--------------------------------------------------------------------------
| This route is used to set your webhook with Telegram,
| just request from your browser once and then disable it.
|
| Example: http://domain.com/bot/set-webhook
*/
Route::get('/bot/set-webhook',
[
    'as' => 'bot-set-webhook',
    'uses' => 'BotController@setWebhook',
    'middleware' => 'admin'
]);


/*
|--------------------------------------------------------------------------
| Remove Webhook
|--------------------------------------------------------------------------
| This route is used to remove your webhook with Telegram,
| just request from your browser once and then disable it.
|
| Example: http://domain.com/bot/remove-webhook
*/
Route::get('/bot/remove-webhook',
[
    'as' => 'bot-remove-webhook',
    'uses' => 'BotController@removeWebhook',
    'middleware' => 'admin'
]);

/*
|--------------------------------------------------------------------------
| Webhook (Incoming Handler)
|--------------------------------------------------------------------------
| This route handles incoming webhook updates.
|
| !! IMPORTANT !!
| THIS IS AN INSECURE ENDPOINT FOR DEMO PURPOSES,
| MAKE SURE TO SECURE THIS ENDPOINT, EXAMPLE: "/bot/<SECRET-KEY>/webhook"
|
| THEN SET THAT WEBHOOK WITH TELEGRAM.
| SO YOU CAN BE SURE THE UPDATES ARE COMING FROM TELEGRAM ONLY.
*/
Route::post('/bot/'.env('TELEGRAM_BOT_TOKEN').'/webhook', 'BotController@webhookHandler')->name('bot-webhook');