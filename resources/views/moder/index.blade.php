@extends('layouts.moder')

@section('content')
<hr>
<h4 class="centered">Список добавленных товаров</h4>
<hr>

<div class="container tableColorWhite">
    <div class="col-lg-12 col-md-12 tableAddedRealization">
        <table class="table table-bordered">
            <tr>
                <th>Дата</th>
                <th>Город</th>
                <th>Район</th>
                <th>Название</th>
                <th>Цена</th>
                <th>Описание</th>
                <th>Фото</th>
            </tr>
            @foreach($realizations as $realization)
                <tr>
                    <td>{{$realization->created_at->format('d.m.Y')}}<br>{{$realization->created_at->format('H:i:s')}}</td>
                    <td>{{$realization->city->title}}</td>
                    <td>{{$realization->district->title}}</td>
                    <td>{{$realization->product->title}}</td>
                    <td>{{$realization->product->price}}</td>
                    <td class="comments-space">{{$realization->description}}</td>
                    <td>
                        <a href="{{secure_asset('storage/'.$realization->image)}}" class="thickbox">
                            <img src="{{secure_asset('storage/'.$realization->thumbnail)}}" class="img-rounded">
                        </a>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
</div><!-- .container -->
@endsection