@extends('layouts.moder')

@section('content')
<hr>
<h4 class="centered">Добавить товар</h4>
<hr>

<div class="container">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6">

            @if(Session::has('message'))
                <div class="codePaddingLeft">
                    <p>
                        <code>{{Session::get('message')}}</code>
                    </p>
                </div>
            @endif

            <form action="{{ route('do-upload') }}" class="form-horizontal" method="POST" enctype="multipart/form-data">

                {{ csrf_field() }}

                <input type="hidden" name="moderId" value="{{Auth::user()->id}}">

                <div class="form-group{{ $errors->has('cityId') ? ' has-error' : '' }}">
                    <strong>Город:</strong>
                    <select id="city" name="cityId" required>
                        <option selected></option>
                        @foreach($cities as $city)
                            <option value="{{$city->id}}">{{$city->title}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('cityId'))
                        <span class="help-block">
                            <strong>{{ $errors->first('cityId') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('districtId') ? ' has-error' : '' }}">
                    <strong>Район:</strong>
                    <select name="districtId" id="districts" required>
                        <option disabled selected>&nbsp;&nbsp;&nbsp;&nbsp;</option>
                    </select>
                    @if ($errors->has('districtId'))
                        <span class="help-block">
                            <strong>{{ $errors->first('districtId') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('productId') ? ' has-error' : '' }}">
                    <strong>Продукт:</strong>
                    <select name="productId" id="" required>
                        <option disabled selected></option>
                        @foreach($products as $product)
                            <option value="{{$product->id}}">{{$product->title}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('productId'))
                        <span class="help-block">
                            <strong>{{ $errors->first('productId') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                    <code>максимум 5MB; 4K Ultra HD (3840x2160) </code>
                    <input type="file" name="image" id="image" required>
                    @if ($errors->has('image'))
                        <span class="help-block">
                            <strong>{{ $errors->first('image') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group indentDesc{{ $errors->has('description') ? ' has-error' : '' }}">
                    <textarea class="form-control descriptionRealization" rows="3" name="description" placeholder="Описание">{{old('description')}}</textarea>
                    @if ($errors->has('description'))
                        <span class="help-block">
                            <strong>{{ $errors->first('description') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group submitRealization">
                    {{--<button type="submit" class="btn btn-default" name="btnUploadRealization" value="preview">Предпросмотр</button>--}}
                    <button type="submit" class="btn btn-primary" name="btnUploadRealization" value="send">Отправить</button>
                </div>
            </form>
        </div>


        @if(session('realization'))
            <div class="col-lg-6 col-md-6 col-sm-6 newRealization">
                <p><strong>ОБЗОР ПРОДУКТА</strong></p>
                <p><strong>Дата: </strong>{{session('realization')->created_at->format('d.m.Y H:i:s')}}</p>
                <p><strong>Город: </strong>{{session('realization')->city->title}}</p>
                <p><strong>Район: </strong>{{session('realization')->district->title}}</p>
                <p><strong>Продукт: </strong>{{session('realization')->product->title}} - {{session('realization')->product->price}} грн.</p>
                <p><strong>Ключ заказа: </strong>{{session('realization')->order_key}}</p>
                <p><strong>Описание: </strong>{{session('realization')->description}}</p>
                <p>
                    <img src="{{secure_asset('storage/'.session('realization')->image)}}" class="img-rounded img-responsive">
                </p>

                <form action="{{route('destroy-realization')}}" method="POST" onsubmit="return confirm('Вы действительно хотите удалить продукт?');">

                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}

                    <input type="hidden" name="id" value="{{session('realization')->id}}">

                    <button type="submit" class="btn btn-danger btn-md" >
                        Удалить
                    </button>
                </form>
            </div>
            <br>
        @endif



    </div><!-- row -->
</div><!-- .container -->

@endsection

