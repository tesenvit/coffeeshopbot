@extends('layouts.admin')

@section('content')
<hr class="hrTopLine">
<h4 class="centered">Районы</h4>
<hr>

@if(Session::has('message'))
    <div class="container">
        <div class="col-lg-12">
            <code>{{Session::get('message')}}</code>
        </div>
    </div>
    <br>
@endif

<div class="container cnt-cities accordionContainer">
    <div class="col-lg-12 accordionColumns">
        @foreach($cities as $city)
            <button class="accordion">{{$city->title}}</button>
            <div class="panelAcc">
                <div class="row">
                    <div class="col-lg-4 col-md-4">
                        <hr>
                        <h4 class="centered">Добавить</h4>
                        <hr>
                        <form class="form-horizontal" method="POST" action="{{route('store-district')}}">

                            {{ csrf_field() }}

                            <input type="hidden" name="cityId" value="{{$city->id}}">

                            <div class="form-group{{ $errors->has('district') ? ' has-error' : '' }}">
                                <div class="col-lg-12 col-md-12">
                                    <input type="text" class="form-control" name="district" id="district" required>
                                    @if ($errors->has('district'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('district') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-2 col-md-2">
                                    <button type="submit" class="btn btn-primary">
                                        Добавить
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="col-lg-4 col-md-4">
                        <hr>
                        <h4 class="centered">Изменить</h4>
                        <hr>
                        @foreach($city->districts as $district)
                            <form action="{{route('update-district')}}" method="POST" class="form-horizontal">

                                {{ csrf_field() }}
                                {{ method_field('PUT') }}

                                <input type="hidden" name="districtId" value="{{$district->id}}">
                                <input type="hidden" name="cityId" value="{{$city->id}}">

                                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                    <div class="col-lg-8 col-md-7 col-sm-9 col-xs-7" >
                                        <input type="text" class="form-control" value="{{$district->title}}" name="title" id="title" required>
                                        @if ($errors->has('title'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('title') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-2">
                                        <button type="submit" class="btn btn-primary btn-md">
                                            Изменить
                                        </button>
                                    </div>
                                </div>
                            </form>
                        @endforeach
                    </div>

                    <div class="col-lg-4 col-md-4">
                        <hr>
                        <h4 class="centered">Удалить</h4>
                        <hr>
                        <div class="bs-callout bs-callout-danger bg-danger">
                            <p class="">
                                Внимание! При удалении района из базы данных будет удалена его реализация.
                            </p>
                        </div>
                        @foreach($city->districts as $district)
                            <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1">
                                <form action="" method="POST" class="form-horizontal" onsubmit="return confirm('Вы действительно хотите удалить район?');">

                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}

                                    <input type="hidden" name="districtId" value="{{$district->id}}">
                                    <input type="hidden" name="cityId" value="{{$city->id}}">

                                    <div class="form-group">
                                        <div class="col-lg-7 col-md-7 col-sm-6 col-xs-6">
                                            <label for="districtDeleteButton" class="verticalAlignDeleteButton">{{$district->title}}</label>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-3 col-xs-6">
                                            <button type="submit" class="btn btn-danger btn-md" id="districtDeleteButton">
                                                Удалить
                                            </button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        @endforeach

                    </div>

                </div><!-- .row -->
            </div><!-- .panel -->
        @endforeach
    </div><!-- .col-lg-12 -->
</div><!-- .container -->
@endsection