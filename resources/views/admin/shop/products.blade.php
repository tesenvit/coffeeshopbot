@extends('layouts.admin')

@section('content')
<hr class="hrTopLine">
<h4 class="centered">Продукты</h4>
<hr>

@if(Session::has('message'))
    <div class="container">
        <div class="col-lg-12">
            <code>{{Session::get('message')}}</code>
        </div>
    </div>
    <br>
@endif

<div class="container cnt-cities accordionContainer">
    <div class="col-lg-12 accordionColumns">
        @foreach($products as $product)
            <button class="accordion">{{$product->title}} - {{$product->price}} грн</button>
            <div class="panelAcc">
                <div class="row">

                    <div class="col-lg-4 col-md-4">
                        <hr>
                        <h4 class="centered">Описание</h4>
                        <hr>
                        <p>Название: <strong>{{$product->title}}</strong></p>
                        <p>Цена: <strong>{{$product->price}}</strong> грн.</p>
                    </div>

                    <div class="col-lg-4 col-md-4">
                        <hr>
                        <h4 class="centered">Изменить</h4>
                        <hr>
                        <form action="{{route('update-product')}}" method="POST" class="form-horizontal">

                            {{ csrf_field() }}
                            {{ method_field('PUT') }}

                            <input type="hidden" name="id" value="{{$product->id}}">

                            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                <div class="col-lg-12">
                                    <label for="title">Название</label>
                                    <input type="text" class="form-control" value="{{ old('title') }}" name="title" id="title">
                                    @if ($errors->has('title'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('title') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                                <div class="col-lg-12">
                                    <label for="title">Цена</label>
                                    <input type="text" class="form-control" value="{{ old('price') }}" name="price">
                                    @if ($errors->has('price'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('price') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-12">
                                    <button type="submit" class="btn btn-primary btn-md">
                                        Изменить
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="col-lg-4 col-md-4">
                        <hr>
                        <h4 class="centered">Удалить</h4>
                        <hr>
                        <div class="bs-callout bs-callout-danger bg-danger">
                            <p>
                                Внимание! При удалении продукта из базы данных будет удалена реализация с названием этого продукта.
                            </p>
                        </div>
                        <form action="" method="POST" onsubmit="return confirm('Вы действительно хотите удалить продукт?');">

                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}

                            <input type="hidden" name="id" value="{{$product->id}}">

                            <button type="submit" class="btn btn-danger btn-md" >
                                Удалить
                            </button>
                        </form>
                    </div>

                </div>
            </div>
        @endforeach
    </div>
</div>


<hr>
<h4 class="centered">Добавить</h4>
<hr>

<div class="container">
    <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2">
        <form class="form-horizontal" method="POST" action="{{route('store-product')}}">

            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                <div class="col-lg-12">
                    <label for="title">Название</label>
                    <input type="text" class="form-control" name="title" id="title" required>
                    @if ($errors->has('title'))
                        <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                <div class="col-lg-12">
                    <label for="price">Цена</label>
                    <input type="text" class="form-control" name="price" id="price" required>
                    @if ($errors->has('price'))
                        <span class="help-block">
                            <strong>{{ $errors->first('price') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-12">
                    <button type="submit" class="btn btn-primary btn-md">
                        Добавить
                    </button>
                </div>
            </div>
        </form>
    </div>
</div><!-- .container -->
@endsection