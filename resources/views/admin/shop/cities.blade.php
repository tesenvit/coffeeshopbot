@extends('layouts.admin')

@section('content')
<hr class="hrTopLine">
<h4 class="centered">Города</h4>
<hr>

@if(Session::has('message'))
    <div class="container">
        <div class="col-lg-12">
            <code>{{Session::get('message')}}</code>
        </div>
    </div>
    <br>
@endif

<div class="container cnt-cities accordionContainer">
    <div class="col-lg-12 accordionColumns">
        @foreach($cities as $city)
            <button class="accordion">{{$city->title}}</button>
            <div class="panelAcc">
                <div class="row">
                    <div class="col-lg-4 col-md-4">
                        <hr>
                        <h4 class="centered">Список районов</h4>
                        <hr>
                        @foreach($city->districts as $district)
                            <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1">
                                <ul>
                                    <li>{{$district->title}}</li>
                                </ul>
                            </div>
                        @endforeach
                    </div>

                    <div class="col-lg-4 col-md-4">
                        <hr>
                        <h4 class="centered">Изменить</h4>
                        <hr>
                        <form action="{{route('update-city')}}" method="POST" class="form-horizontal">

                            {{ csrf_field() }}
                            {{ method_field('PUT') }}

                            <input type="hidden" name="id" value="{{$city->id}}">

                            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                <div class="col-lg-12">
                                    <input type="text" class="form-control" value="{{$city->title}}" name="title" required>
                                    @if ($errors->has('title'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('title') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-12">
                                    <button type="submit" class="btn btn-primary btn-md">
                                        Изменить
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="col-lg-4 col-md-4">
                        <hr>
                        <h4 class="centered">Удалить</h4>
                        <hr>
                        <div class="bs-callout bs-callout-danger bg-danger">
                            <p class="">
                                Внимание! При удалении города из базы данных будут удалены его районы и реализация.
                            </p>
                        </div>
                        <form action="" method="POST" onsubmit="return confirm('Вы действительно хотите удалить город?');">

                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}

                            <input type="hidden" name="id" value="{{$city->id}}">

                            <button type="submit" class="btn btn-danger btn-md" >
                                Удалить
                            </button>
                        </form>
                        <br>
                    </div>
                </div><!-- .row -->
            </div><!-- .panel -->
        @endforeach
    </div><!-- .col-lg-12 -->
</div><!-- .container -->

<hr>
<h4 class="centered">Добавить</h4>
<hr>

<div class="container">
    <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2">
        <form class="form-horizontal" method="POST" action="{{route('store-city')}}">

            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                <div class="col-lg-10 col-md-10">
                    <input type="text" class="form-control" name="city" id="city" required>
                    @if ($errors->has('city'))
                        <span class="help-block">
                            <strong>{{ $errors->first('city') }}</strong>
                        </span>
                    @endif
                </div>
                &nbsp;
                <div class="col-lg-1 col-md-1">
                    <button type="submit" class="btn btn-primary">
                        Добавить
                    </button>
                </div>
            </div>
        </form>
    </div>
</div><!-- .container -->

@endsection
