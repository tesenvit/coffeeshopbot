@extends('layouts.admin')

@section('content')

    <!-- Date picker -->
    <div class="container">
        <div class="col-lg-8 col-md-10 col-sm-12 col-xs-10 col-lg-offset-2 col-md-offset-1 col-sm-offset-0 col-xs-offset-1">
            <form class="form-inline formDatePicker" method="POST">
                {{csrf_field()}}
                <div class="form-group">
                    <strong>Результаты</strong>
                    <label for="from">от</label>
                    <input type="text" id="from" name="from" class="form-control" value="{{$from ?? date('d.m.Y')}}">
                </div>
                <div class="form-group dpTo">
                    <label for="to">&nbsp;до</label>
                    <input type="text" id="to" name="to" class="form-control" value="{{$to ?? date('d.m.Y')}}">
                </div>
                &nbsp;
                <button type="submit" class="btn btn-primary datePickerSubmitBtn">Показать</button>
                &nbsp;
                <a href="/admin" class="btn btn-default">Сброс</a>
            </form>
        </div>
    </div>

    <div class="container tableColorWhite">
        <div class="col-lg-10 col-xs-12 col-lg-offset-1 tableIndex">
            <h4>Продажи</h4>
            <table class="table table-bordered">
            <tr>
                <th>Количество</th>
                <th>Сумма</th>
                <th></th>
            </tr>
            <tr>
                <td>{{$orders['timeIntervalCount']}}</td>
                <td>{{$orders['timeIntervalSum']}}</td>
                <td><a href="{{route('list-orders')}}" class="btn btn-default btn-sm" role="button">Подробнее</a></td>
            </tr>
            </table>
        </div>

        <div class="col-lg-10 col-xs-12 col-lg-offset-1 tableIndex">
            <h4>Товары на реализации</h4>
            <table class="table table-bordered">
                <tr>
                    <th>Количество</th>
                    <th>Сумма</th>
                    <th></th>
                </tr>
                <tr>
                    <td>{{$realization['timeIntervalCount']}}</td>
                    <td>{{$realization['timeIntervalSum']}}</td>
                    <td><a href="{{route('list-realization')}}" class="btn btn-default btn-sm" role="button">Подробнее</a></td>
                </tr>
            </table>
        </div>

        <div class="col-lg-10 col-xs-12 col-lg-offset-1 tableIndex">
            <h4>Новые пользователи</h4>
            <table class="table table-bordered">
                <tr>
                    <th>Количество</th>
                    <th></th>
                </tr>
                <tr>
                    <td>{{$telegramUsers['timeIntervalCount']}}</td>
                    <td><a href="{{route('telegram-users')}}" class="btn btn-default btn-sm" role="button">Подробнее</a></td>
                </tr>
            </table>
        </div>
    </div><!-- /.container -->

    <!-- Separator -->
    <div class="designLine">
        <hr>
    </div>

    <div class="container tableColorWhite">
        <div class="col-lg-10 col-xs-12 col-lg-offset-1 tableIndex">
            <h4>За всё время</h4>
            <table class="table table-bordered tableForSlide">
                <tr>
                    <th></th>
                    <th>Количество</th>
                    <th>Сумма</th>
                </tr>
                <tr>
                    <td><strong>Продажи</strong></td>
                    <td>{{$orders['allCount']}}</td>
                    <td>{{$orders['allSum']}}</td>
                </tr>
                <tr>
                    <td><strong>Реализация</strong></td>
                    <td>{{$realization['allCount']}}</td>
                    <td>{{$realization['allSum']}}</td>
                </tr>
                <tr>
                    <td><strong>Пользователи</strong></td>
                    <td>{{$telegramUsers['allCount']}}</td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div><!-- /.container -->

@endsection