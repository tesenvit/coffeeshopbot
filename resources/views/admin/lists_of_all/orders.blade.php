@extends('layouts.admin')

@section('content')
<hr>
<h4 class="centered">Список проданных товаров</h4>
<hr>

<div class="container tableColorWhite">
    <div class="col-lg-12 tableListAll">
        <p>
            <span>
                <strong>Всего:</strong> {{$ordersCount}} шт.
            </span>,&nbsp;
            <span>
                <strong>на сумму:</strong> {{$ordersSum}} грн.
            </span>
        </p>
        <table class="table table-bordered">
            <tr>
                <th>Дата</th>
                <th>Город</th>
                <th>Район</th>
                <th>Название</th>
                <th>Цена</th>
                <th>Добавил</th>
                <th>Купил</th>
            </tr>
            @foreach($orders as $order)
                <tr>
                    <td>{{$order->created_at->format('d.m.Y')}}<br>{{$order->created_at->format('H:i:s')}}</td>
                    <td>{{$order->city}}</td>
                    <td>{{$order->district}}</td>
                    <td>{{$order->product}}</td>
                    <td>{{$order->price}}</td>
                    <td>
                        <a href="moder/{{$order->user->username ?? ''}}/show">
                            {{$order->user->username ?? '' }}
                        </a>
                    </td>
                    <td>{{$order->telegramUser->first_name}}</td>
                </tr>
            @endforeach
        </table>
        {{$orders->links()}}
    </div>
</div> <!-- .container -->
@endsection