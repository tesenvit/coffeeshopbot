@extends('layouts.admin')

@section('content')
<hr>
<h4 class="centered">Список товаров на реализации</h4>
<hr>

@if(Session::has('message'))
    <div class="container">
        <div class="col-lg-12">
            <code>{{Session::get('message')}}</code>
        </div>
    </div>
    <br>
@endif

<div class="container tableColorWhite">
    <div class="col-lg-12 tableListAll">
        <p>
            <span>
                <strong>Всего:</strong> {{$realizationsCount}} шт.
            </span>,&nbsp;
            <span>
                <strong>на сумму:</strong> {{$realizationsSum}} грн.
            </span>
        </p>
        <table class="table table-bordered">
            <tr>
                <th>Дата</th>
                <th>Город</th>
                <th>Район</th>
                <th>Название</th>
                <th>Цена</th>
                <th>Описание</th>
                <th>Добавил</th>
                <th>Фото</th>
                <th></th>
            </tr>
            @foreach($realizations as $realization)
                <tr>
                    <td>{{$realization->created_at->format('d.m.Y')}}<br>{{$realization->created_at->format('H:i:s')}}</td>
                    <td>{{$realization->city->title}}</td>
                    <td>{{$realization->district->title}}</td>
                    <td>{{$realization->product->title}}</td>
                    <td>{{$realization->product->price}}</td>
                    <td class="comments-space">{{$realization->description}}</td>
                    <td>
                        <a href="moder/{{$realization->user->username ?? ''}}/show">
                            {{$realization->user->username ?? ''}}
                        </a>
                    </td>
                    <td>
                        <a href="{{secure_asset('storage/'.$realization->image)}}" class="thickbox">
                            <img src="{{secure_asset('storage/'.$realization->thumbnail)}}" class="img-rounded">
                        </a>
                    </td>
                    <td>
                        <form action="{{route('delete-realization')}}" method="POST" onsubmit="return confirm('Вы действительно хотите удалить продукт из реализации?');">

                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}

                            <input type="hidden" name="id" value="{{$realization->id}}">

                            <button type="submit" class="btn btn-danger btn-md" >
                                Удалить
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>
        <p></p>{{$realizations->links()}}
    </div>
</div> <!-- .container -->
@endsection