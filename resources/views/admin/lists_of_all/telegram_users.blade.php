@extends('layouts.admin')

@section('content')
<hr>
<h4 class="centered">Список пользователей</h4>
<hr>

<div class="container tableColorWhite">
    <div class="col-lg-12 tableListAll">
        <p><strong>Всего:</strong> {{$telegramUsersCount}} чел.</p>
        <table class="table table-bordered">
            <tr>
                <th>Дата</th>
                <th>ID</th>
                <th>Имя</th>
                <th>Фамилия</th>
                <th>Псевдоним</th>
                <th>Бот?</th>
            </tr>
            @foreach($telegramUsers as $telegramUser)
                <tr>
                    <td>{{$telegramUser->created_at->format('d.m.Y')}}<br>{{$telegramUser->created_at->format('H:i:s')}}</td>
                    <td>{{$telegramUser->telegram_id}}</td>
                    <td>{{$telegramUser->first_name}}</td>
                    <td>{{$telegramUser->last_name}}</td>
                    <td>{{$telegramUser->username ? '@'.$telegramUser->username : ''}}</td>
                    <td>{{$telegramUser->is_bot ? 'Да' : 'Нет'}}</td>
                </tr>
            @endforeach
        </table>
        {{$telegramUsers->links()}}
    </div>
</div> <!-- .container -->
@endsection