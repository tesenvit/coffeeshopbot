@extends('layouts.admin')

@section('content')
<hr>
<h4 class="centered" id="toggleToken">Бот <span id="token" >токен:&nbsp;{{env('TELEGRAM_BOT_TOKEN')}}</span></h4>
<hr>

<div class="container">
    <form action="{{route('on-off-bot')}}" method="POST" id="onOffBotForm">

        {{ csrf_field() }}

        <div class="row">
            <div class="col-lg-3 col-md-4 col-sm-5 col-xs-8">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 onOffBotText">
                    <strong style="">ВЫКЛ/ВКЛ</strong>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 onOffBotTumbler">
                    <label class="switch">
                        <input type="checkbox" name="onOffBot" {{$bot->active ? 'checked' : ''}} id="onOffBot">
                        <span class="slider round"></span>
                    </label>
                </div>
            </div>

            @if(Session::has('messageWebhook'))
                <div class="col-lg-9 col-md-8 col-sm-7 col-xs-5 messageWebhook">
                    <code>{{Session::get('messageWebhook')}}</code>
                </div>
            @endif
        </div><!-- .row-->
    </form>
</div><!-- .container -->

<hr>
<h4 class="centered">Текст стартовой команды</h4>
<hr>

<div class="container">
    @if(Session::has('message'))
        <p><code>{{Session::get('message')}}</code></p>
    @endif

    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6">
            <p><strong>Изменить</strong> <span class="buttonHelperForTags">подсказка по тегам</span></p>
            <div class="textHelperForTags">
                <div><code>Подсказка по использованию HTML тегов, которые допускает Telegram</code></div>
                <br>
                <div class="tableColorWhite">
                    <table class="table table-bordered">
                        <tr>
                            <th>Тэги</th>
                            <th>Результат</th>
                        </tr>
                        <tr>
                            <td>&lt;i&gt;Курсив&lt;/i&gt;</td>
                            <td><i>Курсив</i></td>
                        </tr>
                        <tr>
                            <td>&lt;em&gt;Курсив2&lt;/em&gt;</td>
                            <td><em>Курсив2</em></td>
                        </tr>
                        <tr>
                            <td>&lt;b&gt;Жирность&lt;/b&gt;</td>
                            <td><b>Жирность</b></td>
                        </tr>
                        <tr>
                            <td>&lt;strong&gt;Жирность2&lt;/strong&gt;</td>
                            <td><strong>Жирность2</strong></td>
                        </tr>
                        <tr>
                            <td>&lt;a href=&ldquo;https://www.google.com&rdquo;&gt;Ссылка&lt;/a&gt;</td>
                            <td><a href="https://www.google.com">Ссылка</a></td>
                        </tr>
                        <tr>
                            <td>&lt;a href=&ldquo;tg://user?id=123456789&rdquo;&gt;Ссылка на юзера*&lt;/a&gt;</td>
                            <td><a href="tg://user?id=123456789">Ссылка на юзера*</a></td>
                        </tr>
                        <tr>
                            <td>&lt;code&gt;Выделение кода&lt;/code&gt;</td>
                            <td><code>Выделение кода</code></td>
                        </tr>
                        <tr>
                            <td>&lt;pre&gt;Текст в натуральном виде**&lt;/pre&gt;</td>
                            <td><pre>Текст в натуральном виде**</pre></td>
                        </tr>
                    </table>
                    <p><strong>* Ссылка на юзера</strong> - Упоминание конкретного пользователя Telegram, при клике на ссылку перенаправляет на пользователя чей ID указан в теге</p>
                    <p><strong>** Текст в натуральном виде</strong> - Текст в том виде в каком его написали, тоесть, с сохранением всех пробелов и переносов. В тег &lt;pre&gt; нельзя влаживать другие теги</p>
                </div>
            </div>

            <form action="{{route('update-text-command-start')}}" method="POST">

                {{ csrf_field() }}
                {{ method_field('PUT') }}

                <div class="form-group">
                    <textarea class="form-control textCommandStart" rows="4" name="textCommandStart" onkeydown="expandtext(this);">{{ session('botTextCommandStart') ?? $bot->text_command_start }}</textarea>
                </div>

                <div>
                    <button type="submit" class="btn btn-default" name="btnUpdateCommandStart" value="preview">Предпросмотр</button>
                    <button type="submit" class="btn btn-primary" name="btnUpdateCommandStart" value="update">Изменить</button>
                </div>
                <br>
            </form>
        </div>

        @if(session('botTextCommandStart'))
            <div class="col-lg-6 col-md-6 col-sm-6">
                <p><strong>Предварительный просмотр</strong></p>
                <div class="previewCommandStart">{!! nl2br(session('botTextCommandStart')) !!}</div>
            </div>
        @endif

    </div><!-- .row --->
</div><!-- .container -->

@endsection