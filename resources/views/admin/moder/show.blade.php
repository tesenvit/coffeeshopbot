@extends('layouts.admin')

@section('content')

    <div class="container">
        @if(Session::has('message'))
                <code>{{Session::get('message')}}</code>
        @endif

        <div class="row">

            <div class="col-lg-6 col-md-6">
                <hr>
                <h4 class="centered">Общие сведения</h4>
                <hr>
                <p>ID: <strong>{{$moder->id}}</strong></p>
                <p>Имя: <strong>{{$moder->username}}</strong></p>
                <p>Дата регистрации: <strong>{{$moder->created_at->format('d.m.Y H:i:s')}}</strong></p>
                <p>Дата последнего изменения: <strong>{{$moder->updated_at->format('d.m.Y H:i:s')}}</strong></p>
                <br>
                <form action="" method="POST" onsubmit="return confirm('Вы действительно хотите удалить аккаунт?');">

                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}

                    <input type="submit" value="Удалить аккаунт" class="btn btn-danger deleteModer">
                </form>
            </div>

            <div class="col-lg-6 col-md-6">

                <hr>
                <h4 class="centered">Изменить данные</h4>
                <hr>

                <form class="form-horizontal" method="POST" action="{{ route('update-moder') }}">

                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <input type="hidden" name="role" value="moder">
                    <input type="hidden" name="id" value="{{$moder->id}}">

                    <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                        <label for="username" class="col-md-4 control-label">Имя пользователя</label>

                        <div class="col-lg-6 col-md-6">
                            <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}">

                            @if ($errors->has('username'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('username') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-md-4 control-label">Новый пароль</label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control" name="password">

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password-confirm" class="col-md-4 control-label">Подтвердите пароль</label>
                        <div class="col-md-6">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Изменить
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div> <!-- .row -->
    </div> <!-- .container -->

    <div class="container tableColorWhite">
        <div class="col-lg-12 col-md-12 listAddedRealization">
            <hr>
            <h4 class="centered">Список добавленных товаров</h4>
            <hr>
            <table class="table table-bordered">
                <tr>
                    <th>Дата</th>
                    <th>Город</th>
                    <th>Район</th>
                    <th>Название</th>
                    <th>Цена</th>
                    <th>Описание</th>
                    <th>Фото</th>
                    <th></th>
                </tr>
                @foreach($realizations as $realization)
                    <tr>
                        <td>{{$realization->created_at->format('d.m.Y')}}<br>{{$realization->created_at->format('H:i:s')}}</td>
                        <td>{{$realization->city->title}}</td>
                        <td>{{$realization->district->title}}</td>
                        <td>{{$realization->product->title}}</td>
                        <td>{{$realization->product->price}}</td>
                        <td class="comments-space">{{$realization->description}}</td>
                        <td>
                            <a href="{{secure_asset('storage/'.$realization->image)}}" class="thickbox">
                                <img src="{{secure_asset('storage/'.$realization->thumbnail)}}" class="img-rounded">
                            </a>
                        </td>
                        <td>
                            <form action="{{route('delete-realization')}}" method="POST" onsubmit="return confirm('Вы действительно хотите удалить продукт из реализации?');">

                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}

                                <input type="hidden" name="id" value="{{$realization->id}}">

                                <button type="submit" class="btn btn-danger btn-md" >
                                    Удалить
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div> <!-- .container -->
@endsection