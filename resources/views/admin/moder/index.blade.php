@extends('layouts.admin')

@section('content')
<hr class="hrTopLine">
<h4 class="centered">Список модераторов</h4>
<hr>

@if(Session::has('message'))
    <div class="container">
        <div class="col-lg-12">
            <code>{{Session::get('message')}}</code>
        </div>
    </div>
    <br>
@endif

<div class="container tableColorWhite">
    <div class="col-lg-12 listModers">
        <table class="table table-bordered">
            <tr>
                <th>Дата регистрации</th>
                <th>Имя</th>
                <th>Добавленные товары</th>
                <th></th>
            </tr>
            @foreach($moderators as $moder)
                <tr>
                    <td>{{$moder->created_at->format('d.m.Y')}}</td>
                    <td>{{$moder->username}}</td>
                    <td>{{$moder->realization()->count()}}</td>
                    <td><a href="moder/{{$moder->username}}/show" class="btn btn-default btn-sm" role="button">Подробнее</a></td>
                </tr>
            @endforeach
        </table>
    </div>
</div>
@endsection