<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Admin dashboard</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/admin_dashboard.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">


    {{--thickbox--}}
    <script
            src="https://code.jquery.com/jquery-3.2.1.js"
            integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
            crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{ asset('js/thickbox.js') }}"></script>

    <link href="{{ asset('css/thickbox.css') }}" rel="stylesheet">

</head>
<body>
    <!-- HEADER -->
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <span class="navbar-brand">
                    Администратор
                </span>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    &nbsp;
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                            {{ Auth::user()->username }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu">
                            <li>
                                <a href="{{route('admin-dashboard')}}">Главная</a>
                            </li>

                            <li>
                                <a href="{{route('list-orders')}}">Продажи</a>
                            </li>

                            <li>
                                <a href="{{route('list-realization')}}">Реализация</a>
                            </li>

                            <li>
                                <a href="{{route('telegram-users')}}">Пользователи</a>
                            </li>

                            <li>
                                <hr class="separatorRightNavbar">
                            </li>

                            <li>
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    Выход
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <!-- NAVIGATION MENU -->
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                 <span class="navbar-brand visible-xs">
                    Меню
                </span>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav text-center">

                    <!-- Main page-->
                    <li class="">
                        <a href="{{ route('admin-dashboard') }}">
                            <span class="titleNavbar">Главная</span>
                        </a>
                    </li>

                    <!-- Bot -->
                    <li class="">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <span class="titleNavbar">Бот</span>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ route('index-bot') }}">&bull;&nbsp;Настройки</a></li>
                            <li><a href="{{ route('show-message-everyone') }}">&bull;&nbsp;Сообщение всем</a></li>
                        </ul>
                    </li>

                    <!-- Moder -->
                    <li class="">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <span class="titleNavbar">Модератор</span>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ route('list-moders') }}">&bull;&nbsp;Список</a></li>
                            <li><a href="{{ route('create-moder') }}">&bull;&nbsp;Зарегестрировать</a></li>
                        </ul>
                    </li>

                    <!-- Shop -->
                    <li class="">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <span class="titleNavbar">Магазин</span>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="{{route('cities')}}">&bull;&nbsp;Города</a></li>
                            <li><a href="{{route('districts')}}">&bull;&nbsp;Районы</a></li>
                            <li><a href="{{route('products')}}">&bull;&nbsp;Продукты</a></li>
                        </ul>
                    </li>

                    <!-- Payment system -->
                    <li>
                        <a href="#">
                            <span class="titleNavbar">Платежная система</span>
                        </a>
                    </li>

                    <!-- Profile -->
                    <li>
                        <a href="{{route('profile')}}">
                            <span class="titleNavbar">Профиль</span>
                        </a>
                    </li>

                </ul>
            </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
    </nav>

    @yield('content')

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/admin_dashboard.js') }}"></script>

    {{--Date picker--}}
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

</body>
</html>
