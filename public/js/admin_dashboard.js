/* Date picker */
$( function() {
    var monthNamesShort = [
        "Янв","Фев","Мар","Апр","Май","Июн",
        "Июл","Авг","Сен","Окт","Ноя","Дек"
    ];
    var dayNamesMin = [
        'Вс','Пн','Вт','Ср','Чт','Пт','Сб'
    ];
    var dateFormat = "dd.mm.yy",
        from = $( "#from" )
            .datepicker({
                defaultDate: "+0w",
                changeMonth: true,
                numberOfMonths: 1,
                monthNamesShort: monthNamesShort,
                dayNamesMin: dayNamesMin,
                firstDay: 1,
                dateFormat: 'dd.mm.yy',
                maxDate:0
            })
            .on( "change", function() {
                to.datepicker( "option", "minDate", getDate( this ) );
            }),
        to = $( "#to" ).datepicker({
            defaultDate: "+0w",
            changeMonth: true,
            numberOfMonths: 1,
            monthNamesShort: monthNamesShort,
            dayNamesMin: dayNamesMin,
            firstDay: 1,
            dateFormat: 'dd.mm.yy',
            maxDate:0
        })
            .on( "change", function() {
                from.datepicker( "option", "maxDate", getDate( this ) );
            });

    function getDate( element ) {
        var date;
        try {
            date = $.datepicker.parseDate( dateFormat, element.value );
        } catch( error ) {
            date = null;
        }

        return date;
    }
});

/* Bot token toggle */
$( "#toggleToken" ).click(function() {
    $( "#token" ).slideToggle( 50 );
});

/* Helper for HTML tags */
$( ".buttonHelperForTags" ).click(function() {
    $( ".textHelperForTags" ).slideToggle( 50 );
});

/* Shorten text for description at list uploaded products */
var showChar = 9;
var ellipsestext = "...";
var moretext = "Дальше";
var lesstext = "Скрыть";
$('.comments-space').each(function () {
    var content = $(this).html();
    if (content.length > showChar) {
        var show_content = content.substr(0, showChar);
        var hide_content = content.substr(showChar, content.length - showChar);
        var html = show_content + '<span class="moreelipses">' + ellipsestext + '</span>' +
            '<span class="remaining-content"><span>' + hide_content + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
        $(this).html(html);
    }
});

$(".morelink").click(function () {
    if ($(this).hasClass("less")) {
        $(this).removeClass("less");
        $(this).html(moretext);
    } else {
        $(this).addClass("less");
        $(this).html(lesstext);
    }
    $(this).parent().prev().toggle();
    $(this).prev().toggle();
    return false;
});


/*Accordion*/
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        this.classList.toggle("activeAcc");
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }

    });
}

/* Bot checkbox on-off */
$(function(){
    $('#onOffBot').on('change',function(){
        $('#onOffBotForm').submit();
    });
});

/* Text command start of bot*/
$(function() {
    $('.textCommandStart').each(function() {
        $(this).height($(this).prop('scrollHeight'));
    });
});