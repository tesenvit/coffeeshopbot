/* Shorten text for description at list uploaded products */
var showChar = 9;
var ellipsestext = "...";
var moretext = "Дальше";
var lesstext = "Скрыть";
$('.comments-space').each(function () {
    var content = $(this).html();
    if (content.length > showChar) {
        var show_content = content.substr(0, showChar);
        var hide_content = content.substr(showChar, content.length - showChar);
        var html = show_content + '<span class="moreelipses">' + ellipsestext + '</span>' +
            '<span class="remaining-content"><span>' + hide_content + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
        $(this).html(html);
    }
});

$(".morelink").click(function () {
    if ($(this).hasClass("less")) {
        $(this).removeClass("less");
        $(this).html(moretext);
    } else {
        $(this).addClass("less");
        $(this).html(lesstext);
    }
    $(this).parent().prev().toggle();
    $(this).prev().toggle();
    return false;
});

/* Get list districts for city */
$("#city").change(function () {
    $.ajax({
        type: 'GET',
        url: 'list-districts',
        data: 'cityId='+$(this).val(),
        success: function (districts) {
            var dropdown = "";
            $.each(districts, function(index, district) {
                dropdown += "<option value='" + district['id'] + "'>" + district['title'] + "</option>";
            });
            $('#districts').html(dropdown);
        },
        error: function () {
            var errorText = "Произошла ошибка сервера";
            $('#districts').html(errorText);
        }
    });
});