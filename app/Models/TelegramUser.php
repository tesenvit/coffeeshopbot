<?php

namespace App\Models;

use App\Models\MainBotModel;

class TelegramUser extends MainBotModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'telegram_users';

    /**
     * The table primary key
     *
     * @var string
     */
    protected $primaryKey = 'telegram_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'telegram_id','first_name','last_name','username','is_bot'
    ];

    /**
     * Relationship
     *
     */
    public function orders()
    {
        return $this->hasMany('App\Models\Order','telegram_id');
    }
}
