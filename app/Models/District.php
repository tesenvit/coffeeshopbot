<?php

namespace App\Models;

use App\Models\City;
use App\Models\Realization;
use App\Models\MainBotModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class District extends MainBotModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'city_id'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Relationship
     *
     */
    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }

    public function realization()
    {
        return $this->hasMany('App\Models\Realization');
    }

    /**
     * Create a new district
     *
     * @param  Request $request
     * @return void
     */
    static public function createDistrict($request)
    {
        $cityId = $request->cityId;
        $districtTitle = $request->district;

        $cityTitle = City::find($cityId)->title;

        self::create([
            'title' => $districtTitle,
            'city_id' => $cityId
        ]);

        Session::flash('message',"Район '$districtTitle' успешно добавлен в город '$cityTitle'");
    }

    /**
     * Update a district
     *
     * @param  Request $request
     * @return mixed
     */
    static public function updateDistrict($request)
    {
        $district = District::find($request->districtId);
        $district->title = $request->title;
        $district->save();

        $cityTitle = City::find($request->cityId)->title;
        Session::flash('message',"В городе '$cityTitle' успешно измененно название района");

        return redirect()->back();
    }


    /**
     * Destroy a district and his products(realization)
     *
     * @param object Request $request
     *
     * @return void
     */
    static public function deleteDistrict($request)
    {
        self::destroyImage('district_id',$request->districtId);

        Realization::where('district_id',$request->districtId)->delete();

        self::destroy($request->districtId);

        $cityTitle = City::find($request->cityId)->title;
        Session::flash('message',"В городе '$cityTitle' успешно удален район и его продукты");
    }
}
