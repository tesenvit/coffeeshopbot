<?php

namespace App\Models;

use App\Models\MainBotModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class Bot extends MainBotModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'bot';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'active', 'text_command_start', 'number_of_buttons'
    ];

    /**
     * Update text for command start
     *
     * @param  Request $request
     * @return string
     */
    static public function updateTextStartCommand($request)
    {
        $botTextCommandStart = $request->textCommandStart;

        if ($request->btnUpdateCommandStart == 'update')
        {
            $bot = self::find(1);
            $bot->text_command_start = $botTextCommandStart;
            $bot->save();
            $botTextCommandStart = $bot->text_command_start;
            Session::flash('message','Текст стартовой команды успешно изменен');
        }

        return $botTextCommandStart;
    }
}
