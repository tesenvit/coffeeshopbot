<?php

namespace App\Models;

use App\Models\MainBotModel;

class Order extends MainBotModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'telegram_id', 'city', 'district', 'product', 'price', 'order_key', 'user_id'
    ];

    /**
     * Relationship
     *
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function telegramUser()
    {
        return $this->belongsTo('App\Models\TelegramUser','telegram_id','telegram_id');
    }
}
