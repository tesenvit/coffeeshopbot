<?php

namespace App\Models;

use App\Models\Realization;
use Extensions\CustomCollection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class MainBotModel extends Model
{
    /**
     * Extension Collection
     *
     * @param array $models
     *
     * @return object CustomCollection
     */
    public function newCollection(array $models = Array())
    {
        return new CustomCollection($models);
    }

    /**
     * Delete images and thumbnails
     *
     * @param string $field
     * @param integer $id
     *
     * @return void
     */
    static public function destroyImage($field,$id)
    {
        $realizations = Realization::where($field,$id)->get();
        $listImages = $realizations->pluck('image')->toArray();
        $listThumbnails = $realizations->pluck('thumbnail')->toArray();
        $listImagesThumbnails = array_merge($listImages,$listThumbnails);

        Storage::disk('public')->delete($listImagesThumbnails);
    }


}
