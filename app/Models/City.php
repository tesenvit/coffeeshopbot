<?php

namespace App\Models;

use App\Models\District;
use App\Models\Realization;
use App\Models\MainBotModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class City extends MainBotModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Relationship
     *
     */
    public function districts()
    {
        return $this->hasMany('App\Models\District');
    }

    public function realization()
    {
        return $this->hasMany('App\Models\Realization');
    }

    /**
     * Create a new city
     *
     * @param  Request $request
     *
     * @return void
     */
    static public function createCity($request)
    {
        self::create(['title'=>$request->city]);
        Session::flash('message','Город успешно добавлен');
    }

    /**
     * Update a city
     *
     * @param  Request $request
     *
     * @return void
     */
    static public function updateCity( $request)
    {
        $city = self::find($request->id);
        $city->title = $request->title;
        $city->save();

        Session::flash('message','Название города успешно изменено');
    }

    /**
     * Destroy a city and his districts
     *
     * @param  object Request $request
     * @return void
     */
    static public function deleteCity($request)
    {
        self::destroyImage('city_id',$request->id);
        District::where('city_id',$request->id)->delete();
        Realization::where('city_id',$request->id)->delete();
        self::destroy($request->id);
        Session::flash('message','Город и его районы успешно удалены');
    }
}
