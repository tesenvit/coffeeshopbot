<?php

namespace App\Models;

use App\Models\Realization;
use App\Models\MainBotModel;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Session;

class Product extends MainBotModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title','price'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Relationship
     *
     */
    public function realizations()
    {
        return $this->hasMany('App\Models\Realization');
    }

    public function realization()
    {
        return $this->belongsTo('App\Models\Realization');
    }

    /**
     * Update data for Product
     *
     * @param array $request
     *
     * @return object $product
     */
    static public function updateProduct($request)
    {
        $id = $request['id'];
        $title = $request['title'];
        $price = $request['price'];

        $product = self::find($id);

        if($title)
        {
            $product->title = $title;
            Session::flash('message','Название продукта успешно изменено');
        }

        if($price)
        {
            $product->price = $price;
            Session::flash('message','Цена продукта успешно изменена');
        }

        if($title && $price)
            Session::flash('message','Название и цена успешно изменены');

        $product->save();

        return $product;
    }

    /**
     * Delete product
     *
     * @param  object Request $request
     *
     * @return void
     */
    static public function deleteProduct($request)
    {
        self::destroyImage('product_id',$request->id);
        Realization::where('product_id',$request->id)->delete();
        self::find($request->id)->delete();
        Session::flash('message','Продукт и реализация с этим названием успешно удалены');
    }


    /**
     * Create a new product
     *
     * @param  Request $request
     *
     * @return void
     */
    static public function createProduct($request)
    {
        self::create([
            'title' => $request->title,
            'price' => $request->price
        ]);

        Session::flash('message','Продукт успешно добавлен');
    }

    /**
     * Get the collection with strings of the title plus price
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    static public function getAllTitlePlusPrice()
    {
        $products = self::all()->mapWithKeys(function ($item){
            return [$item['id'] => $item['title'].' - '.$item['price'].'грн'];
        });

        return $products;
    }
}