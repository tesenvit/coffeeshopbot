<?php

namespace App\Models;

use App\Models\MainBotModel;
use Illuminate\Database\Eloquent\Model;

class PreOrder extends MainBotModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pre_order';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'telegram_id', 'city', 'district', 'product'
    ];

    /**
     * The table primary key
     *
     * @var string
     */
    protected $primaryKey = 'telegram_id';

    /**
     * Increment the primary key
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * Relationship
     *
     */
    public function realization()
    {
        return $this->hasOne('App\Models\Realization','order_key','order_key');
    }
}
