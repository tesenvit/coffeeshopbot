<?php

namespace App\Models;

use App\Models\MainBotModel;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class Realization extends MainBotModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'realization';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','city_id','district_id','product_id','image','thumbnail','order_key','reserve','description'
    ];

    /**
     * Relationship
     *
     */
    public function district()
    {
        return $this->belongsTo('App\Models\District');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

    public function preOrder()
    {
        return $this->belongsTo('App\Models\PreOrder','order_key','order_key');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }

    /**
     * Storage new realization
     *
     * @param object Request
     *
     * @return object $realization
     */
    static public function doUpload($request)
    {
        $orderKey = uniqid();

        $pathImage = self::storageImage($request->image);

        $realization = self::create([
            'user_id' => $request->moderId,
            'city_id' => $request->cityId,
            'district_id' => $request->districtId,
            'product_id' => $request->productId,
            'image' => $pathImage,
            'thumbnail' => 'thumbnails/'.$pathImage,
            'order_key' => $orderKey,
            'reserve' => false,
            'description' => $request->description
        ]);

        Session::flash('message','Новый продукт успешно добавлен на реализацию');

        return $realization;
    }

    /**
     * Storage new image for realization
     *
     * @param object UploadedFile
     *
     * @return string $path
     */
    static private function storageImage($file)
    {
        $normalWidth = 800; // px
        $normalHeight = 800;

        $thumbWidth = 50; // px
        $thumbHeight = 50;

        $path = $file->hashName();
        $pathThumb = $file->hashName('thumbnails');

        $image = Image::make($file);

        if($image->width() < $image->height())
        {
            $normalWidth = null;
        }
        else
        {
            $normalHeight = null;
        }

        $croppedImage = $image->resize($normalWidth, $normalHeight,  function($constraint){
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        Storage::disk('public')->put($path,$croppedImage->encode('jpg',60));

        self::storageThumbnail($croppedImage,$pathThumb,$thumbWidth,$thumbHeight);

        return $path;
    }

    /**
     * Storage new thumbnail for realization
     *
     * @param object \Intervention\Image\Image
     * @param string $path
     * @param integer $width
     * @param integer $height
     *
     * @return void
     */
    static private function storageThumbnail($image,$path,$width,$height)
    {
        $thumbImage = Image::make($image)->resize($width,$height);
        Storage::disk('public')->put($path,$thumbImage->encode('jpg',100));
    }

    /**
     * Delete realization and image
     *
     * @param integer
     *
     * @return void
     */
    static public function destroyProduct($id)
    {
        $realization = self::find($id);
        Storage::disk('public')->delete([$realization->image, $realization->thumbnail]);
        $realization->delete();
        Session::flash('message','Продукт успешно удален из реализации');
    }
}