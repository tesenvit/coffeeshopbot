<?php

namespace App\Models;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'password', 'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Relationship
     *
     */
    public function realization()
    {
        return $this->hasMany('App\Models\Realization');
    }

    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }

    /**
     * Check if the user is an administrator
     *
     * @return bool
     */
    public function isAdmin()
    {
        if($this->role == 'admin')
        {
            return true;
        }

        return false;
    }

    /**
     * Check if the user is an moderator
     *
     * @return bool
     */
    public function isModer()
    {
        if($this->role == 'moder')
        {
            return true;
        }

        return false;
    }

    /**
     * Update data for user
     *
     * @param  Request $request
     *
     * @return object $user
     */
    static public function updateUser($request)
    {
        $id = $request->id;
        $username = $request->username;
        $password = $request->password;

        $user = self::find($id);

        if($username)
        {
            $user->username = $username;
            Session::flash('message','Имя пользователя успешно изменено');
        }

        if($password)
        {
            $user->password = bcrypt($password);
            Session::flash('message','Пароль пользователя успешно изменен');
        }

        if($username && $password)
            Session::flash('message','Пароль и имя пользователя успешно изменены');

        $user->save();

        return $user;
    }

    /**
     * Create a new moderator
     *
     * @param  Request $request
     *
     * @return void
     */
    static public function createModer($request)
    {
        self::create([
            'username' => $request->username,
            'role' => $request->role,
            'password' => bcrypt($request->password)
        ]);

        Session::flash('message','Пользователь успешно зарегестрирован');
    }

    /**
     * Delete moder
     *
     * @param  string
     * @return void
     */
    static public function deleteModer($username)
    {
        $moder = self::where('username',$username)->first();
        $moder->delete();

        Session::flash('message','Пользователь успешно удален');
    }
}
