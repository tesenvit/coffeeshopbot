<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;

class RegisterAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $admin = User::where('role','admin')->get();
        if($admin->isEmpty())
        {
            return $next($request);
        }

        return abort(404);
    }
}
