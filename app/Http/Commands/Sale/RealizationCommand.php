<?php

namespace App\Commands;

use App\Models\District;
use App\Models\PreOrder;
use App\Models\Product;
use App\Models\Realization;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Keyboard\Keyboard;

/**
 * Class RealizationCommand
 */
class RealizationCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'realization';

    /**
     * {@inheritdoc}
     */
    public function handle($arguments)
    {
        $update = $this->getUpdate()->getMessage();
        $id = $update->getFrom()->getId();

        $preOrder = PreOrder::where('telegram_id', $id)->first();

        // Check if the button was pressed Back
        $districtTitle = $preOrder->district ?? $update->getText();
        $district = District::where('title',$districtTitle)->first();

        $preOrder->district = $district->title;
        $preOrder->district_id = $district->id;

        // If was pressed button - Back
        if($preOrder->order_key)
        {
            $pastRealization = $preOrder->realization;
            $pastRealization->reserve = false;
            $pastRealization->save();

            $preOrder->order_key = null;
        }
        $preOrder->save();

        $realization = Realization::where('city_id', $preOrder->city_id)
                                  ->where('district_id',$preOrder->district_id)
                                  ->where('reserve', false)
                                  ->get()
                                  ->getTitlePlusPrice();

        if (!empty($realization))
        {
            $this->replyWithMessage([
                'text' => '<b>Выберите товар:</b>',
                'reply_markup' => $this->makeKeyboard($realization),
                'parse_mode' => 'HTML'
            ]);
        }
        else
        {
            $text = "<b>В районе {$preOrder->district} закончился товар</b>";

            $this->replyWithMessage([
                'text' => $text,
                'parse_mode' => 'HTML',
            ]);

            $this->triggerCommand('district');
        }
    }

    private function makeKeyboard(array $buttons)
    {
        // Divide the array into subarrays for keyboard
        $keyboard = array_chunk($buttons, 2);

        // Add a button to the end of the keyboard
        array_push($keyboard, ['Назад']);

        $replyMarkup = Keyboard::make([
            'keyboard' => $keyboard,
            'resize_keyboard' => true,
            'one_time_keyboard' => false,
        ]);

        return $replyMarkup;
    }
}