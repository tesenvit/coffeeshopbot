<?php

namespace App\Commands;

use App\Models\PreOrder;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Keyboard\Keyboard;

/**
 * Class PaymentCommand
 */
class PaymentCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'payment';

    /**
     * {@inheritdoc}
     */
    public function handle($arguments)
    {
        $update = $this->getUpdate()->getMessage();
        $id = $update->getFrom()->getId();
        $secretCode = $update->getText();

        $preOrder = PreOrder::where('telegram_id', $id)->first();
        $reserve = $preOrder->realization->reserve;

        if($reserve)
        {
            if($secretCode == '123456')
            {
                $text = '';
                $text .= 'START PAYMENT SYSTEM'. PHP_EOL;
                $text .= '-+-+-+-+-+-'. PHP_EOL;
                $text .= '-+-+-+-'. PHP_EOL;
                $text .= '-+-'. PHP_EOL;
                $text .= 'STOP PAYMENT SYSTEM';

                $this->replyWithMessage([
                    'text' => $text,
                    'parse_mode' => 'HTML'
                ]);

                $this->triggerCommand('result');
            }
            else
            {
                $this->replyWithMessage([
                    'text' => '<b>Вы ввели не верный код</b>',
                    'parse_mode' => 'HTML'
                ]);
            }
        }
        else
        {
            $text = '<b>Время резерва истекло</b>'.PHP_EOL;
            $text .= '<b>Начните сначала</b>';

            $this->replyWithMessage([
                'text' => $text,
                'parse_mode' => 'HTML',
                'reply_markup' => $this->makeKeyboard()
            ]);
        }
    }

    private function makeKeyboard()
    {
        $keyboard = [
            ['Начать сначала']
        ];

        $replyMarkup = Keyboard::make([
            'keyboard' => $keyboard,
            'resize_keyboard' => true,
            'one_time_keyboard' => false,
        ]);

        return $replyMarkup;
    }
}