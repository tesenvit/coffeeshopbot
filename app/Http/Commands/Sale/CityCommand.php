<?php

namespace App\Commands;

use App\Models\Realization;
use App\Models\City;
use App\Models\PreOrder;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Keyboard\Keyboard;

/**
 * Class CityCommand
 */
class CityCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'city';

    /**
     * {@inheritdoc}
     */
    public function handle($arguments)
    {

        $update = $this->getUpdate()->getMessage();
        $id = $update->getFrom()->getId();

        $preOrder = PreOrder::firstOrCreate(['telegram_id' => $id]);

        // Array cities title
        $cities = Realization::where('reserve',false )
                             ->with('city')
                             ->get()
                             ->pluck('city.title')
                             ->unique()
                             ->toArray();

        if(!empty($cities))
        {
            // If was pressed button Back
            if ($preOrder->city)
            {
                $preOrder->city = null;
                $preOrder->save();
            }

            $this->replyWithMessage([
                'text' => '<b>Выберите город:</b>',
                'reply_markup' => $this->makeKeyboard($cities),
                'parse_mode' => 'HTML'
            ]);
        }
        else
        {
            $text = "<b>На данный момент реализация не осуществляется</b>";

            $this->replyWithMessage([
                'text' => $text,
                'parse_mode' => 'HTML'
            ]);
        }

    }

    private function makeKeyboard(array $buttons)
    {
        // Divide the array into subarrays for keyboard
        $keyboard = array_chunk($buttons, 2);

        // Add a button to the end of the keyboard
        array_push($keyboard,['Назад']);

        $replyMarkup = Keyboard::make([
            'keyboard'          => $keyboard,
            'resize_keyboard'   => true,
            'one_time_keyboard' => false,
            'selective'         => true,
        ]);

        return $replyMarkup;
    }
}