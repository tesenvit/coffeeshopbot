<?php

namespace App\Commands;

use App\Models\PreOrder;
use App\Models\Product;
use App\Models\Realization;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Keyboard\Keyboard;

/**
 * Class StartCommand
 */
class PreOrderCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'preorder';

    /**
     * {@inheritdoc}
     */
    public function handle($arguments)
    {
        $update = $this->getUpdate()->getMessage();
        $id = $update->getFrom()->getId();
        $selectedRealization = $update->getText();

        // Identify which product was selected
        $products = Product::getAllTitlePlusPrice();
        $productId = array_search($selectedRealization, $products->toArray());

        $preOrder = PreOrder::where('telegram_id', $id)->first();

        $realization = Realization::where('product_id',$productId)
                                  ->where('city_id', $preOrder->city_id)
                                  ->where('district_id',$preOrder->district_id)
                                  ->where('reserve', false)
                                  ->oldest('updated_at')
                                  ->first();

        if (!empty($realization))
        {
            $realization->reserve = true;
            $realization->save();

            $preOrder->order_key = $realization->order_key;
            $preOrder->save();

            $text = "";
            $text .= "Ваш заказ:" . PHP_EOL;
            $text .= "Город: <b>$preOrder->city</b>" . PHP_EOL;
            $text .= "Район: <b>$preOrder->district</b>" . PHP_EOL;
            $text .= "Продукт: <b>{$realization->product->title}</b>" . PHP_EOL;
            $text .= "Цена: <b>{$realization->product->price} грн</b>" . PHP_EOL;
            $text .= "ID заказа: <b>$preOrder->order_key</b>" . PHP_EOL;
            $text .= "<i>-+-+-+-+-</i>" . PHP_EOL;
            $text .= "Чтобы оплатить товар, проследуйте инструкциям:" . PHP_EOL;
            $text .= "1) Первая инструкция" . PHP_EOL;
            $text .= "2) Вторая инструкция" . PHP_EOL;
            $text .= "<i>-+-+-+-+-</i>" . PHP_EOL;
            $text .= "Мы зарезервировали вашу покупку {$realization->product->title} на <b>15 минут</b>" . PHP_EOL;

            $this->replyWithMessage([
                'text' => $text,
                'reply_markup' => $this->makeKeyboard(),
                'parse_mode' => 'HTML'
            ]);
        }
        else
        {
            $this->replyWithMessage([
                'text' => "<b>Кто-то только что купил этот товар</b>",
                'parse_mode' => 'HTML'
            ]);

            $this->triggerCommand('realization');
        }
    }

    private function makeKeyboard()
    {
        $keyboard = [
            ['Отменить']
        ];

        $replyMarkup = Keyboard::make([
            'keyboard' => $keyboard,
            'resize_keyboard' => true,
            'one_time_keyboard' => false,
        ]);

        return $replyMarkup;
    }
}