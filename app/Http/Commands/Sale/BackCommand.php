<?php

namespace App\Commands;

use App\Models\PreOrder;
use Telegram\Bot\Commands\Command;

/**
 * Class BackCommand
 */
class BackCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'back';

    /**
     * {@inheritdoc}
     */
    public function handle($arguments)
    {
        $update = $this->getUpdate()->getMessage();
        $id = $update->getFrom()->getId();

        $preOrder = PreOrder::where('telegram_id', $id)->first();

        if ($preOrder) {
            if ($preOrder->order_key) {
                return $this->triggerCommand('realization');
            }

            if ($preOrder->district) {
                return $this->triggerCommand('district');
            }

            if ($preOrder->city) {
                return $this->triggerCommand('city');
            }

            if ($preOrder->telegram_id) {
                return $this->triggerCommand('start');
            }
        }

        return $this->triggerCommand('start');
    }
}