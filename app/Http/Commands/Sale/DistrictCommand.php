<?php

namespace App\Commands;

use App\Models\City;
use App\Models\PreOrder;
use App\Models\Realization;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Keyboard\Keyboard;

/**
 * Class DistrictCommand
 */
class DistrictCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'district';

    /**
     * {@inheritdoc}
     */
    public function handle($arguments)
    {
        $update = $this->getUpdate()->getMessage();
        $id = $update->getFrom()->getId();

        $preOrder = PreOrder::where('telegram_id',$id)->first();

        // Check if the button was pressed Back
        $cityTitle = $preOrder->city ?? $update->getText();

        $city = City::where('title', $cityTitle)->first();

        $districts = Realization::where('reserve',false )
                                ->where('city_id',$city->id)
                                ->with('district')
                                ->get()
                                ->pluck('district.title')
                                ->unique()
                                ->toArray();

        if($districts)
        {
            $preOrder->city = $city->title;
            $preOrder->city_id = $city->id;

            // If was pressed button - Back
            if($preOrder->district)
            {
                $preOrder->district = null;
                $preOrder->district_id = null;
            }
            $preOrder->save();

            $this->replyWithMessage([
                'text' => '<b>Выберите район:</b>',
                'reply_markup' => $this->makeKeyboard($districts),
                'parse_mode' => 'HTML'
            ]);
        }
        else
        {
            $text = "<b>В городе {$cityTitle} закончился товар</b>";

            $this->replyWithMessage([
                'text' => $text,
                'parse_mode' => 'HTML'
            ]);

            $this->triggerCommand('city');
        }
    }

    private function makeKeyboard(array $buttons)
    {
        // Divide the array into subarrays for keyboard
        $keyboard = array_chunk($buttons, 2);

        // Add a button to the end of the keyboard
        array_push($keyboard,['Назад']);

        // Options keyboard
        $replyMarkup = Keyboard::make([
            'keyboard'          => $keyboard,
            'resize_keyboard'   => true,
            'one_time_keyboard' => false,
            'selective'         => true,
        ]);

        return $replyMarkup;
    }
}