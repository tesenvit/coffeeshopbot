<?php

namespace App\Commands;

use App\Models\Order;
use App\Models\PreOrder;
use App\Models\Realization;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Keyboard\Keyboard;
use Illuminate\Support\Facades\Storage;

/**
 * Class ResultCommand
 */
class ResultCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'result';

    /**
     * {@inheritdoc}
     */
    public function handle($arguments)
    {
        $update = $this->getUpdate()->getMessage();
        $id = $update->getFrom()->getId();

        $realization = PreOrder::where('telegram_id', $id)
                               ->first()
                               ->realization;

        $this->replyWithPhoto([
            'photo' => secure_asset('storage/' . $realization->image),
            'reply_markup' => $this->makeKeyboard()
        ]);

        $this->replyWithMessage([
            'text' => $realization->description
        ]);

        Order::create([
            'telegram_id' => $id,
            'city' => $realization->city->title,
            'district' => $realization->district->title,
            'product' => $realization->product->title,
            'price' => $realization->product->price,
            'order_key' => $realization->order_key,
            'user_id' => $realization->user_id
        ]);

        // Delete a realization and it image
        Realization::destroyProduct($realization->id);

        // Delete an entry from the Pre Order
        PreOrder::destroy($id);
    }

    private function makeKeyboard()
    {
        $keyboard = [
            ['Начать сначала']
        ];

        $replyMarkup = Keyboard::make([
            'keyboard' => $keyboard,
            'resize_keyboard' => true,
            'one_time_keyboard' => false,
        ]);

        return $replyMarkup;
    }
}