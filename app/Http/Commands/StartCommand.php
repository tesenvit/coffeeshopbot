<?php

namespace App\Commands;

use App\Models\Bot;
use App\Models\PreOrder;
use App\Models\TelegramUser;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Keyboard\Keyboard;

/**
 * Class StartCommand
 */
class StartCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'start';

    /**
     * {@inheritdoc}
     */
    public function handle($arguments)
    {
        // Data of User
        $update = $this->getUpdate();
        $user = $update->getMessage()->getFrom(); //User object
        $id = $user->getId();
        $firstName = $user->getFirstName();
        $lastName = $user->getLastName(); //maybe null
        $username = $user->getUsername(); //maybe null
        $isBot = $user->getIsBot();

        TelegramUser::firstOrCreate(
        [
        'telegram_id' => $id
        ],
        [
            'first_name' => $firstName,
            'last_name' => $lastName,
            'username' => $username,
            'is_bot' => $isBot
        ]);

        // Cancel pre-order if it exists
        PreOrder::destroy($id);

        // Message Text
        $text = Bot::find(1)->text_command_start;

        // Message options
        $this->replyWithMessage([
            'text' => $text,
            'reply_markup' => $this->makeKeyboard(),
            'parse_mode' => 'HTML'
        ]);
    }

    private function makeKeyboard()
    {
        // Buttons
        $keyboard = [
            ['Купить','Последние покупки'],
        ];

        // Options keyboard
        $replyMarkup = Keyboard::make([
            'keyboard'          => $keyboard,
            'resize_keyboard'   => true,
            'one_time_keyboard' => false,
            'selective'         => true,
        ]);

        return $replyMarkup;
    }
}