<?php

namespace App\Commands;

use Telegram\Bot\Commands\Command;

/**
 * Class ErrorCommand
 */
class ErrorCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'error';

    /**
     * {@inheritdoc}
     */
    public function handle($arguments)
    {
        $this->replyWithMessage([
            'text' => 'Ты втираешь мне какую-то <b>дичь!</b>',
            'parse_mode' => 'HTML'
        ]);
    }
}