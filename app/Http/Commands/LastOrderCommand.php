<?php

namespace App\Commands;

use App\Models\Order;
use Telegram\Bot\Commands\Command;

/**
 * Class LastOrderCommand
 */
class LastOrderCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'lastorder';

    /**
     * {@inheritdoc}
     */
    public function handle($arguments)
    {
        $update = $this->getUpdate();
        $user = $update->getMessage()->getFrom();
        $id = $user->getId();

        $orders = Order::where('telegram_id', $id);

        $count = $orders->count();

        $lastOrders = $orders->latest()
                             ->take(5)
                             ->get()
                             ->reverse();

        $text = "";
        if (!$lastOrders->isEmpty()) {

            $text .= $this->getTitleText($count).PHP_EOL;
            $text .= "<i>-+-+-</i>".PHP_EOL;

            foreach ($lastOrders as $order)
            {
                $text .= "Город: <b>$order->city</b>".PHP_EOL;
                $text .= "Район: <b>$order->district</b>".PHP_EOL;
                $text .= "Продукт: <b>$order->product</b>".PHP_EOL;
                $text .= "Цена: <b>$order->price грн</b>".PHP_EOL;

                $date = $order->created_at->format('d.m.Y');

                $text .= "Число: <b>$date</b>".PHP_EOL;
                $text .= "<i>-+-+-</i>".PHP_EOL;
            }
            $text .= "Всего: <b>$count</b>".PHP_EOL;
        }
        else
        {
            $text .= '<b>Вы не совершили ни одной покупки</b>';
        }

        $this->replyWithMessage([
            'text' => $text,
            'parse_mode' => 'HTML'
        ]);
    }

    private function getTitleText($count)
    {
        $text = "";

        if($count == 1)
        {
            $text .= "Последняя покупка:";
        }
        else
        {
            $text .= "Последние покупки:";
        }

        return $text;
    }


}