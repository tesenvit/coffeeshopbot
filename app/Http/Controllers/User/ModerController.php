<?php

namespace App\Http\Controllers\User;

use App\Models\City;
use App\Models\Product;
use App\Models\District;
use App\Models\Realization;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\RealizationUploadRequest;

class ModerController extends Controller
{

    /**
     * Show list of products for realization
     *
     * @return mixed
     */
    public function index()
    {
        $realizations = Realization::where('user_id', Auth::user()->id)->get()->reverse();
        return view('moder.index',compact('realizations'));
    }

    /**
     * Show form for upload product
     *
     * @return mixed
     */
    public function upload()
    {
        $cities = City::all();
        $products = Product::all();
        return view('moder.upload',compact('cities','products'));
    }

    /**
     * Upload new product for realization
     *
     * @param RealizationUploadRequest $request
     *
     * @return mixed
     */
    public function doUpload(RealizationUploadRequest $request)
    {
        $realization = Realization::doUpload($request);
        return redirect()->back()->with('realization', $realization);
    }

    /**
     * Destroy a realization
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function destroyRealization(Request $request)
    {
        Realization::destroyProduct($request->id);
        return redirect()->back();
    }

    /**
     * List districts for AJAX request
     *
     * @param Request $request
     *
     * @return Response json
     */
    public function listDistricts(Request $request)
    {
        $districts =  District::where('city_id',$request->cityId)->get();
        return response()->json($districts, 200);
    }
}
