<?php

namespace App\Http\Controllers\User;

use App\Models\PreOrder;
use App\Models\Bot;
use App\Models\City;
use App\Models\User;
use App\Models\Order;
use App\Models\Product;
use App\Models\District;
use App\Models\Realization;
use App\Models\TelegramUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\DatePickerRequest;
use App\Http\Requests\CityUpdateRequest;
use App\Http\Requests\CityCreateRequest;
use App\Http\Requests\ModerCreateRequest;
use App\Http\Requests\ModerUpdateRequest;
use App\Http\Requests\AdminUpdateRequest;
use App\Http\Requests\ProductCreateRequest;
use App\Http\Requests\ProductUpdateRequest;
use App\Http\Requests\DistrictCreateRequest;
use App\Http\Requests\DistrictUpdateRequest;
use App\Http\Requests\BotUpdateTextCommandStart;

class AdminController extends Controller
{
    /**
     * Show data in the time interval
     *
     * @param  DatePickerRequest  $request
     * @return mixed
     */
    public function index(DatePickerRequest $request)
    {
        $from = $request->get('from');
        $to = $request->get('to');

        // Get data for orders
        $dataOrders = Order::all();
        $orders = [
            'allCount' => $dataOrders->count(),
            'allSum' => $dataOrders->sum('price'),
            'timeIntervalCount' => $dataOrders->getDataBetween($from,$to)->count(),
            'timeIntervalSum' => $dataOrders->getDataBetween($from,$to)->sum('price'),
        ];

        // Get data for realizations
        $dataRealization = Realization::all();
        $realization = [
            'allCount' => $dataRealization->count(),
            'allSum' => $dataRealization->getRealizationSum(),
            'timeIntervalCount' => $dataRealization->getDataBetween($from,$to)->count(),
            'timeIntervalSum' => $dataRealization->getDataBetween($from,$to)->getRealizationSum(),
        ];

        // Get data for telegram users
        $dataTelegramUsers = TelegramUser::all();
        $telegramUsers = [
            'allCount' => $dataTelegramUsers->count(),
            'timeIntervalCount' => $dataTelegramUsers->getDataBetween($from,$to)->count()
        ];

        return view('admin.index', compact('from','to', 'orders','realization','telegramUsers'));
    }

    /**
     * Show form for change of password and username
     *
     * @return mixed
     */
    public function profileShow()
    {
        return view('admin.profile');
    }

    /**
     * Update admin data
     *
     * @param  AdminUpdateRequest  $request
     * @return mixed
     */
    public function updateProfile(AdminUpdateRequest $request)
    {
        User::updateUser($request);
        return redirect()->back();
    }

    /**
     * List of all orders
     *
     * @return mixed
     */
    public function listOrders()
    {
        $ordersAll = Order::all();
        $ordersCount = $ordersAll->count();
        $ordersSum = $ordersAll->sum('price');

        $orders = Order::orderBy('created_at', 'desc')->paginate(10);
        return view('admin.lists_of_all.orders',compact('orders','ordersCount', 'ordersSum'));
    }

    /**
     * List of all realization
     *
     * @return mixed
     */
    public function listRealization()
    {
        $realizationsAll = Realization::all();
        $realizationsCount = $realizationsAll->count();
        $realizationsSum = $realizationsAll->getRealizationSum();

        $realizations = Realization::orderBy('created_at', 'desc')->paginate(10);
        return view('admin.lists_of_all.realization',compact('realizations','realizationsCount','realizationsSum'));
    }

    /**
     * List of all telegram users
     *
     * @return mixed
     */
    public function listTelegramUsers()
    {
        $telegramUsersCount = TelegramUser::all()->count();

        $telegramUsers = TelegramUser::orderBy('created_at', 'desc')->paginate(10);
        return view('admin.lists_of_all.telegram_users',compact('telegramUsers','telegramUsersCount'));
    }

    /**
     * Delete product from realization
     *
     * @param object Request $request
     *
     * @return mixed
     */
    public function destroyRealization(Request $request)
    {
        Realization::destroyProduct($request->id);
        return redirect()->back();
    }

    /**
     * Show list of all districts
     *
     * @return mixed
     */
    public function shopDistricts()
    {
        $cities = City::all();
        return view('admin.shop.districts',compact('cities'));
    }

    /**
     * Create a new district
     *
     * @param  DistrictCreateRequest  $request
     * @return mixed
     */
    public function shopStoreDistrict(DistrictCreateRequest $request)
    {
        District::createDistrict($request);
        return redirect()->back();
    }

    /**
     * Update a district
     *
     * @param  DistrictUpdateRequest  $request
     * @return mixed
     */
    public function shopUpdateDistrict(DistrictUpdateRequest $request)
    {
        District::updateDistrict($request);
        return redirect()->back();
    }

    /**
     * Destroy a district and his products(realization)
     *
     * @param  Request $request
     * @return mixed
     */
    public function shopDestroyDistrict(Request $request)
    {
        District::deleteDistrict($request);
        return redirect()->back();
    }

    /**
     * Show list of all cities
     *
     * @return mixed
     */
    public function shopCities()
    {
        $cities = City::all();
        return view('admin.shop.cities',compact('cities'));
    }

    /**
     * Create a new city
     *
     * @param  CityCreateRequest  $request
     * @return mixed
     */
    public function shopStoreCity(CityCreateRequest $request)
    {
        City::createCity($request);
        return redirect()->back();
    }

    /**
     * Update a city
     *
     * @param  CityUpdateRequest $request
     * @return mixed
     */
    public function shopUpdateCity(CityUpdateRequest $request)
    {
        City::updateCity($request);
        return redirect()->back();
    }

    /**
     * Destroy a city and his districts
     *
     * @param  Request  $request
     * @return mixed
     */
    public function shopDestroyCity(Request $request)
    {
        City::deleteCity($request);
        return redirect()->back();
    }

    /**
     * Show all products
     *
     * @return mixed
     */
    public function shopProducts()
    {
        $products = Product::all();
        return view('admin.shop.products',compact('products'));
    }

    /**
     * Create a new product
     *
     * @param  ProductCreateRequest  $request
     * @return mixed
     */
    public function shopStoreProduct(ProductCreateRequest $request)
    {
//        City::createCity($request);
        Product::createProduct($request);
        return redirect()->back();
    }

    /**
     * Update product data
     *
     * @param  ProductUpdateRequest  $request
     * @return mixed
     */
    public function shopUpdateProduct(ProductUpdateRequest $request)
    {
        Product::updateProduct($request->all());
        return redirect()->back();
    }

    /**
     * Delete product
     *
     * @param  Request $request
     * @return mixed
     */
    public function shopDestroyProduct(Request $request)
    {
        Product::deleteProduct($request);
        return redirect()->back();
    }

    /**
     * Show all moderators
     *
     */
    public function moderIndex()
    {
        $moderators = User::where('role','moder')
                          ->get()
                          ->reverse();

        return view('admin.moder.index',compact('moderators'));
    }

    /**
     * Show concrete moderator
     *
     * @param string
     *
     * @return mixed
     */
    public function moderShow($username)
    {
        $moder = User::where('role','moder')
                     ->where('username',$username)
                     ->first();

        if($moder)
        {
            $realizations = $moder->realization()
                                  ->get()
                                  ->reverse();
            return view('admin.moder.show',compact('moder','realizations'));
        }
        else
        {
            return redirect('admin/moder');
        }

    }

    /**
     * Form for create new moder
     *
     */
    public function moderCreate()
    {
        return view('admin.moder.create');
    }

    /**
     * Create a new moderator
     *
     * @param  ModerCreateRequest  $request
     * @return mixed
     */
    public function moderStore(ModerCreateRequest $request)
    {
        User::createModer($request);
        return redirect('admin/moder/');
    }

    /**
     * Update moder data
     *
     * @param  ModerUpdateRequest  $request
     * @return mixed
     */
    public function moderUpdate(ModerUpdateRequest $request)
    {
        $moder = User::updateUser($request);

        $path = 'admin/moder/'.$moder->username.'/show';
        return redirect($path);
    }

    /**
     * Delete moder
     *
     * @param  string
     *
     * @return mixed
     */
    public function moderDestroy($username)
    {
        User::deleteModer($username);
        return redirect('admin/moder/');
    }

    /**
     * Show main page for settings Bot
     *
     * @return mixed
     */
    public function botIndex()
    {
        $bot = Bot::find(1);
        return view('admin.bot.index',compact('bot','text'));
    }

    /**
     * Update text of command 'start'
     *
     * @param  BotUpdateTextCommandStart $request
     * @return mixed
     */
    public function botUpdateCommandStart(BotUpdateTextCommandStart $request)
    {
        $botTextCommandStart = Bot::updateTextStartCommand($request);
        return redirect()->back()->with('botTextCommandStart', $botTextCommandStart);
    }

    /**
     * Show form for send message everyone
     *
     * @return mixed
     */
    public function botSendMessageEveryone()
    {
        return view('admin.bot.send_message');
    }
}
