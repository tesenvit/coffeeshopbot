<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Carbon\Carbon;
use App\Models\Bot;
use App\Models\City;
use Telegram\Bot\Api;
use App\Models\District;
use App\Models\PreOrder;
use App\Models\Realization;
use Illuminate\Http\Request;
use App\Models\TelegramUser;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\SendMessageEveryoneRequest;

class BotController extends Controller
{
    /** @var Api */
    protected $telegram;

    /**
     * BotController constructor.
     *
     * @param Api $telegram
     */
    public function __construct(Api $telegram)
    {
        $this->telegram = $telegram;
    }

    /**
     * Enables and disables the bot
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function onOffTumbler(Request $request)
    {
        if($request->onOffBot == 'on')
        {
            $this->setWebhook();
        }
        else
        {
            $this->removeWebhook();
        }

        return redirect()->back();
    }

    /**
     * Enables of bot
     * Installs the webhook
     *
     * @return void
     */
    protected function setWebhook()
    {
        // https://api.telegram.org/bot<BOT TOKEN>/setWebhook?url=<APP URL>/bot/<BOT TOKEN>/webhook

        $this->telegram->setWebhook([
            'url' => env('APP_URL').'/bot/'.env('TELEGRAM_BOT_TOKEN').'/webhook'
        ]);

        $bot = Bot::find(1);
        $bot->active = true;
        $bot->save();

        Session::flash('messageWebhook','Бот включен');
    }

    /**
     * Disable of bot
     * Remove the webhook
     *
     * @return void
     */
    protected function removeWebhook()
    {
        $this->telegram->removeWebhook();

        $bot = Bot::find(1);
        $bot->active = false;
        $bot->save();

        Session::flash('messageWebhook','Бот выключен');
    }

    /**
     * Webhook handler
     *
     * @return string response code
     */
    public function webhookHandler()
    {
        // Enable the command system
        $update = $this->telegram->commandsHandler(true);

        // Get message text and telegram ID
        $text = $update->getMessage()->getText();
        $id = $update->getMessage()->getFrom()->getId();

        $this->checkReserve(); // fifteen minutes

       // Determine whether the text is a bot command
        $command = $this->generatorCommand($text,$id);

       // Call the right command
        $this->telegram->triggerCommand($command,$update);

        return 'Ok';
    }

    /**
     * Generates a command regarding what the user entered
     *
     * @param $text
     * @param $id
     *
     * @return string text command
     */
    private function generatorCommand($text,$id)
    {
        // Reserved commands
        if($text == '/start')
            return '';

        if($text == 'Начать сначала' OR $text == 's' OR $text == 'S')
            return 'start';

        if($text == 'Купить')
            return 'city';

        if($text == 'Назад' OR $text == 'Отменить')
            return 'back';

        if($text == 'Последние покупки')
            return 'lastorder';

        $preOrder = PreOrder::where('telegram_id',$id)->first();

        if($preOrder)
        {
            // if there was no activity
            if($preOrder->updated_at->diffInMinutes(Carbon::now()) > 60) // <- 60 minutes
            {
                return 'start';
            }

            if(preg_match('/[а-яё -]{3,21}/iu',$text))
            {
                $city = City::where('title',$text)->first();
                if($city AND !$preOrder->district AND !$preOrder->order_key)
                {
                    return 'district';
                }

                $district = District::where('title',$text)->first();
                if($district AND $preOrder->city AND !$preOrder->order_key)
                {
                    return 'realization';
                }
            }

            if(preg_match('/[\s\w\.,]{3,15} - [\d]{1,5}грн/u',$text))
            {
                $products = Product::getAllTitlePlusPrice();
                $result = array_search($text, $products->toArray());
                if($result AND $preOrder->district AND $preOrder->city AND !$preOrder->order_key)
                {
                    return 'preorder';
                }
            }

            if(preg_match('/[0-9]{6,10}/',$text))
            {
                if($preOrder->order_key)
                {
                    return 'payment';
                }
            }
        }

        return 'error';
    }

    /**
     * Check if some time has passed after the order
     *
     * @return void
     */
    private function checkReserve()
    {
        $realizations = Realization::where('reserve',true)->get();

        if(!empty($realizations))
        {
            foreach ($realizations as $realization)
            {
                if($realization->updated_at->diffInMinutes(Carbon::now()) > 15) // <= minutes
                {
                    $realization->reserve = false;
                    $realization->save();
                }
            }
        }
    }

    /**
     * Send message everyone users
     *
     * @param SendMessageEveryoneRequest $request
     *
     * @return mixed
     */
    public function sendMessageEveryone(SendMessageEveryoneRequest $request)
    {
        $message = $request['messageEveryone'];

        if ($request['btnSendMessage'] == 'send')
        {
            $users = TelegramUser::all();
            foreach ($users as $user)
            {
                try
                {
                    $this->telegram->sendMessage([
                        'chat_id' => $user->telegram_id,
                        'text' => $message,
                        'parse_mode' => 'HTML'
                    ]);
                }
                catch (\Exception $e)
                {
                    continue;
                }
            }

            Session::flash('message','Сообщение отправлено всем пользователям');
        }

        return redirect()->back()->with('sendMessageEveryone',$message);
    }
}
