<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RealizationUploadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cityId' => 'required|integer',
            'districtId' => 'required|integer',
            'productId' => 'required|integer',
            'image' => 'required|image|max:5120',
            'description' => 'required|max:1000',
            'btnUploadRealization' => [
                'required',
                'regex:/^(send|preview)$/'
            ]
        ];
    }
}
