<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DatePickerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // format  dd.mm.yyyy  25.12.2017
        return [
            'from' => 'regex:/^[0-9]{2}\.[0-9]{2}\.20[0-9]{2}$/',
            'to' => 'regex:/^[0-9]{2}\.[0-9]{2}\.20[0-9]{2}$/'
        ];
    }
}
