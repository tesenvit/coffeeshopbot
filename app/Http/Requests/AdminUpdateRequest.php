<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

class AdminUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'role' => 'required|regex:/^admin/',
            'username' => 'nullable|string|max:20|unique:users',
            'password' => 'nullable|string|min:6|confirmed',
            'old_password' => 'nullable|old_password:' . Auth::user()->password
        ];
    }
}
