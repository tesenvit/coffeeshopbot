<?php

namespace Extensions;

use Carbon\Carbon;
use App\Models\Product;
use Illuminate\Database\Eloquent\Collection as Collection;

class CustomCollection extends Collection
{
    /**
     * Select data between two dates
     *
     * @param  string  $from
     * @param  string  $to
     *
     * @return CustomCollection
     */
    public function getDataBetween($from, $to)
    {
        if($from == null)
        {
            $from = Carbon::now()->toDateString();
        }

        if($to == null)
        {
            $to = Carbon::now()->toDateString();
        }

        $start = Carbon::parse($from)->startOfDay();
        $end = Carbon::parse($to)->endOfDay();

        return $this->where('created_at','>=',$start)
            ->where('created_at','<=',$end);
    }

    /**
     * The amount of the price tag of goods on sale
     *
     * Only for Realization model
     * @return int
     */
    public function getRealizationSum()
    {
        try
        {
            $result = '';
            foreach ($this as $realization)
            {
                $result += $realization->product()->first()->price;
            }

            if($result < 1)
            {
                return 0;
            }

            return $result;
        }
        catch(\Exception $e)
        {
            return null;
        }
    }

    /**
     * Get the array title plus price
     *
     * @return array
     */
    public function getTitlePlusPrice()
    {
        $realization = $this->pluck('product_id')->toArray();

        $products = Product::find($realization)->mapWithKeys(function ($item){
            return [$item['id'] => $item['title'].' - '.$item['price'].'грн'];
        });

        return $products->toArray();
    }
}